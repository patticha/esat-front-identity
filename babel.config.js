
const plugins = [
    [
        'babel-plugin-transform-imports',
        {
            '@material-ui/core': {
                transform: '@material-ui/core/esm/${member}',
                preventFullImport: true
            },
            '@material-ui/icons': {
                transform: '@material-ui/icons/esm/${member}',
                preventFullImport: true
            }
        }
    ],
    [
        'react-intl',
        {
            idInterpolationPattern: '[sha512:contenthash:base64:6]',
            extractFromFormatMessageCall: true,
            ast: true
        }
    ],
    ['@babel/plugin-proposal-optional-chaining'],
    ['dynamic-import-node']
];

module.exports = {
    presets: [
        '@babel/preset-env',
        '@babel/preset-react'
    ],
    plugins
};
