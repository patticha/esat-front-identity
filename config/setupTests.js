import { configure, mount, render, shallow } from 'enzyme';
import { mountWithIntl, shallowWithIntl } from '../src/Helpers/test-helper';
import toJson from 'enzyme-to-json';
import Adapter from 'enzyme-adapter-react-16';
import React from 'react';
import fetch from 'unfetch';
import 'babel-polyfill';

configure({ adapter: new Adapter() });

global.shallow = shallow;
global.toJson = toJson;
global.mountWithIntl = mountWithIntl;
global.shallowWithIntl = shallowWithIntl;
global.render = render;
global.mount = mount;
global.React = React;
global.fetch = fetch;
