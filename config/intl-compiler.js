exports.compile = function (msgs) {
    const results = {};
    for (const msg of msgs) {
        results[msg.id] = msg.defaultMessage;
    }

    return results;
};
