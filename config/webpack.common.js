const webpack = require('webpack');
const path = require('path');
const HtmlWebPackPlugin = require('html-webpack-plugin');
const paths = require('./paths');

const DIST_PATH = 'dist';

const entry = path.resolve(__dirname, '../src/entry.js');
const filename = process.env.NODE_ENV === 'production' ? 'js/bundle.min.js' : 'js/bundle.dev.js';
const domain = process.env.NODE_ENV === 'production' ? '.e-satisfaction.com' : '.e-satisfaction.localhost';

let host; let cdn; let baseURL;
switch (process.env.NODE_ENV) {
    case 'production':
        host = paths.hosts.PRODUCTION;
        cdn = paths.cdn.PRODUCTION;
        baseURL = paths.baseURL.PRODUCTION;
        break;
    case 'development':
        host = paths.hosts.DEVELOPMENT;
        cdn = paths.cdn.DEVELOPMENT;
        baseURL = paths.baseURL.DEVELOPMENT;
        break;
    case 'stage':
        host = paths.hosts.STAGE;
        cdn = paths.cdn.STAGE;
        baseURL = paths.baseURL.STAGE;
        break;
    default:
        host = paths.hosts.PRODUCTION;
        cdn = paths.cdn.PRODUCTION;
        baseURL = paths.baseURL.PRODUCTION;
}

module.exports = {
    paths: {
        entry,
        public: path.resolve(__dirname, `../${DIST_PATH}`),
        src: path.resolve(__dirname, '../src'),
        publicPath: `${cdn}`
    },
    output: {
        filename
    },
    devServer: {
        historyApiFallback: true
    },
    module: {
        rules: [
            {
                test: /\.(js|jsx)$/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader'
                }
            },
            {
                test: /\.(woff|woff2|eot|ttf|otf)$/,
                use: [
                    'file-loader'
                ]
            },
            {
                test: /\.(png|jpg|gif|svg)$/i,
                use: [
                    {
                        loader: 'file-loader',
                        options: {
                            limit: 8192,
                            name: '[name].[ext]',
                            outputPath: 'images'
                        }
                    }
                ]
            },
            {
                test: /\.html$/,
                use: [
                    {
                        loader: 'html-loader'
                    }
                ]
            },
            {
                test: /\.s[ac]ss$/i,
                use: [
                    'style-loader',
                    'css-loader',
                    'sass-loader'
                ]
            }

        ]
    },
    plugins: [
        new webpack.DefinePlugin({
            PRODUCTION: process.env.NODE_ENV === 'production',
            DEBUG: process.env.NODE_ENV === 'development' || process.env.NODE_MODE === 'debug',
            HOST: JSON.stringify(host),
            CDN: JSON.stringify(cdn),
            BASE_URL: JSON.stringify(baseURL),
            DOMAIN: JSON.stringify(domain)
        }),
        new HtmlWebPackPlugin({
            filename: path.join(__dirname, '../dist', 'index.html'),
            template: path.join(__dirname, '../src', 'index.html')
        })
    ]
};
