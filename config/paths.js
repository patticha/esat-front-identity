module.exports = {
    cdn: {
        PRODUCTION: 'https://cdn.e-satisfaction.com',
        DEVELOPMENT: '/',
        STAGE: 'http://cdn.qa.e-satisfaction.com'
    },
    baseURL: {
        PRODUCTION: 'https://api.esatisfaction.eu',
        DEVELOPMENT: 'http://api.esatisfaction.localhost:8000',
        STAGE: 'https://api.qa.esatisfaction.eu'
    },
    hosts: {
        PRODUCTION: 'https://id.e-satisfaction.com',
        DEVELOPMENT: 'http://id.e-satisfaction.localhost:8080',
        STAGE: 'https://id.qa.e-satisfaction.com'
    }
};
