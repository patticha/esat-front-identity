import {
    USER_REGISTER,
    GET_LATEST_TERMS
} from '../ActionTypes';
import registerReducer from './registerReducer';

let action;
beforeEach(() => {
    action = {
        type: undefined,
        payload: {}
    };
});

const initialState = {
    terms: {
        version: undefined,
        url: 'https://www.e-satisfaction.com/home/terms-conditions/'
    },
    status: undefined,
    errorMessage: undefined
};

describe('[Store] RegisterReducer:', () => {
    it('Should return initial state', () => {
        expect(registerReducer(undefined, {})).toEqual(initialState);
    });

    it('Should return default state for unhandled action type', () => {
        action.type = 'doesnexist';
        expect(registerReducer(undefined, action)).toEqual(initialState);
    });

    it('Should handle default REJECTED USER REGISTER', () => {
        const mockResponse = {
            response: {
                status: 1000
            }
        };
        action.type = USER_REGISTER + '_REJECTED';
        action.payload = mockResponse;
        expect(registerReducer(undefined, action)).toEqual({
            ...initialState
        });

    });

    it('Should handle USER REGISTER status ACCEPTED', () => {
        action.type = USER_REGISTER + '_FULFILLED';

        expect(registerReducer(undefined, action)).toEqual({
            ...initialState,
            status: 'accepted'
        });
    });

    it('Should handle REJECTED USER REGISTER with response 409', () => {
        const mockResponse = {
            response: {
                status: 409
            }
        };
        action.type = USER_REGISTER + '_REJECTED';
        action.payload = mockResponse;
        expect(registerReducer(undefined, action)).toEqual({
            ...initialState,
            status: 'error',
            errorMessage: 'User already exists with the same email'
        });

    });

    it('Should handle GET LATEST TERMS status ACCEPTED', () => {
        const mockResponse = {
            data: {
                version: 'v1.00',
                url: 'www.e-satifaction/terms/v1.00'
            }
        };
        action.type = GET_LATEST_TERMS + '_FULFILLED';
        action.payload = mockResponse;

        expect(registerReducer(undefined, action)).toEqual({
            ...initialState,
            terms: {
                version: mockResponse.data.version,
                url: mockResponse.data.url
            }
        });
    });
});
