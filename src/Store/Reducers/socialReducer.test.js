import {
    FACEBOOK_CONNECT,
    GOOGLE_CONNECT
} from '../ActionTypes';
import socialReducer from './socialReducer';

let action;
beforeEach(() => {
    action = {
        type: undefined,
        payload: {}
    };
});

const initialState = {
    user: {},
    status: undefined,
    errorMessage: undefined
};

describe('[Store] SocialReducer:', () => {
    it('Should return initial state', () => {
        expect(socialReducer(undefined, {})).toEqual(initialState);
    });

    it('Should handle FACEBOOK status ACCEPTED', () => {
        const mockResponse = {
            data: {
                token: 'token'
            }
        };
        action.type = FACEBOOK_CONNECT + '_FULFILLED';
        action.payload = mockResponse;

        expect(socialReducer(undefined, action)).toEqual({
            ...initialState,
            status: 'accepted',
            user: mockResponse.data

        });
    });

    it('Should handle _REJECTED FACEBOOK', () => {
        action.type = FACEBOOK_CONNECT + '_REJECTED';
        expect(socialReducer(undefined, action)).toEqual({
            ...initialState,
            status: 'error'
        });

    });

    it('Should handle GOOGLE status ACCEPTED', () => {
        const mockResponse = {
            data: {
                token: 'token'
            }
        };
        action.type = GOOGLE_CONNECT + '_FULFILLED';
        action.payload = mockResponse;

        expect(socialReducer(undefined, action)).toEqual({
            ...initialState,
            status: 'accepted',
            user: mockResponse.data

        });
    });

    it('Should handle _REJECTED GOOGLE', () => {
        action.type = GOOGLE_CONNECT + '_REJECTED';
        expect(socialReducer(undefined, action)).toEqual({
            ...initialState,
            status: 'error'
        });

    });
});
