import {
    STATUS_PENDING,
    STATUS_ACCEPTED,
    STATUS_ERROR,
    ERRORS,
    DEFAULT_LOCALE
} from '../../Utils/constants';
import {
    APP_CLEAR_STORE,
    USER_LOGIN,
    APP_GET_IP_GEO
} from '../ActionTypes';
import { Mixpanel } from '../../Helpers/Mixpanel';

const initialState = {
    user: {
        locale: navigator.language || DEFAULT_LOCALE,
        // eslint-disable-next-line new-cap
        timeZone: Intl?.DateTimeFormat?.().resolvedOptions?.().timeZone,
        twoFactor: false,
        isAuthenticated: false
    },
    status: undefined,
    errorMessage: undefined
};

export default function(state = initialState, action) {
    switch (action.type) {
        case USER_LOGIN + '_FULFILLED': {
            const { data } = action?.payload;
            const { status, ...user } = data;
            switch (status) {
                case STATUS_PENDING: {
                    Mixpanel.track('[FORM] Login Pending');

                    return {
                        ...state,
                        status: STATUS_PENDING,
                        user: {
                            ...user,
                            twoFactor: true
                        }
                    };
                }

                case STATUS_ACCEPTED: {
                    Mixpanel.track('[FORM] Login Accepted');
                    return {
                        ...state,
                        status: STATUS_ACCEPTED,
                        user: {
                            ...user,
                            isAuthenticated: true,
                            twoFactor: false
                        }
                    };
                }

                default: {
                    return state;
                }
            }
        }

        case USER_LOGIN + '_REJECTED': {
            let { response } = action.payload;
            Mixpanel.track(`[Form] Login Failed ${response?.status}`);
            switch (response?.status) {
                case 401: {
                    return {
                        ...state,
                        status: STATUS_ERROR,
                        errorMessage: state?.user?.twoFactor ? ERRORS.LOGIN_2_FACTOR[401] : ERRORS.LOGIN[401],
                        user: {
                            ...state.user,
                            isAuthenticated: false,
                            twoFactor: false
                        }
                    };
                }

                case 404: {
                    return {
                        ...state,
                        status: STATUS_ERROR,
                        errorMessage: ERRORS.LOGIN[404]
                    };
                }

                default:
                    return state;
            }

        }

        case APP_CLEAR_STORE: {
            return {
                ...initialState,
                user: {
                    ...state.user,
                    timeZone: state.user.timeZone,
                    locale: state.user.locale
                }
            };
        }

        case APP_GET_IP_GEO + '_PENDING': {
            return {
                ...state,
                user: {
                    ...state.user,
                    loading: true
                }
            };
        }

        case APP_GET_IP_GEO + '_FULFILLED': {
            const { languages, time_zone: timeZone } = action?.payload;
            const [firstLang] = languages?.split?.(',');
            return {
                ...state,
                user: {
                    ...state.user,
                    locale: firstLang || state.user.locale,
                    timeZone: timeZone?.name || state.user.timeZone,
                    loading: false
                }
            };
        }

        default:
            return state;
    }
}
