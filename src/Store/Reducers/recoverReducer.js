import {
    STATUS_PENDING,
    STATUS_ACCEPTED,
    STATUS_ERROR,
    ERRORS
} from '../../Utils/constants';
import {
    PASSWORD_RECOVER,
    PASSWORD_RESET
} from '../ActionTypes';
import { Mixpanel } from '../../Helpers/Mixpanel';

const initialState = {
    recover: {
        status: STATUS_PENDING,
        errorMessage: undefined
    },
    reset: {
        status: STATUS_PENDING,
        errorMessage: undefined
    }

};

export default function(state = initialState, action) {
    switch (action.type) {
        case PASSWORD_RECOVER + '_FULFILLED': {
            Mixpanel.track('[FORM] Password Recovery Accepted');

            return {
                ...state,
                recover: {
                    ...state.recover,
                    status: STATUS_ACCEPTED
                }
            };
        }

        case PASSWORD_RECOVER + '_REJECTED': {
            let { response } = action.payload;
            Mixpanel.track(`[FORM] Password Recovery Failed ${response?.status}`);

            switch (response?.status) {
                case 404: {
                    return {
                        ...state,
                        recover: {
                            ...state.recover,
                            status: STATUS_ERROR,
                            errorMessage: ERRORS.RECOVER[404]
                        }
                    };
                }

                default:
                    return state;
            }

        }

        case PASSWORD_RESET + '_FULFILLED': {
            Mixpanel.track('[FORM] Password Reset Accepted');

            return {
                ...state,
                reset: {
                    ...state.reset,
                    status: STATUS_ACCEPTED
                }
            };
        }

        case PASSWORD_RESET + '_REJECTED': {
            let { response } = action.payload;
            Mixpanel.track(`[FORM] Password Reset Failed ${response?.status}`);

            switch (response?.status) {
                case 400: {
                    return {
                        ...state,
                        reset: {
                            ...state.recover,
                            status: STATUS_ERROR,
                            errorMessage: ERRORS.RESET[400]
                        }
                    };
                }

                default:
                    return state;
            }

        }

        default:
            return state;
    }
}
