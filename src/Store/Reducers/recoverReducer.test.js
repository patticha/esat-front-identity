import {
    PASSWORD_RECOVER,
    PASSWORD_RESET
} from '../ActionTypes';
import recoverReducer from './recoverReducer';

let action;
beforeEach(() => {
    action = {
        type: undefined,
        payload: {}
    };
});

const initialState = {
    recover: {
        status: 'pending',
        errorMessage: undefined
    },
    reset: {
        status: 'pending',
        errorMessage: undefined
    }

};
describe('[Store] RecoverReducer:', () => {
    it('Should return initial state', () => {
        expect(recoverReducer(undefined, {})).toEqual(initialState);
    });

    it('Should return default state for unhandled action type', () => {
        action.type = 'doesnexist';
        expect(recoverReducer(undefined, action)).toEqual(initialState);
    });

    it('Should handle default REJECTED PASSWORD RESET', () => {
        const mockResponse = {
            response: {
                status: 1000
            }
        };
        action.type = PASSWORD_RESET + '_REJECTED';
        action.payload = mockResponse;
        expect(recoverReducer(undefined, action)).toEqual({
            ...initialState
        });

    });

    it('Should handle default REJECTED PASSWORD RECOVER', () => {
        const mockResponse = {
            response: {
                status: 1000
            }
        };
        action.type = PASSWORD_RECOVER + '_REJECTED';
        action.payload = mockResponse;
        expect(recoverReducer(undefined, action)).toEqual({
            ...initialState
        });

    });

    it('Should handle PASSWORD RECOVER status ACCEPTED', () => {
        const mockResponse = {
            data: {
                status: 'accepted',
                token: 'user-token'
            }
        };
        action.type = PASSWORD_RECOVER + '_FULFILLED';
        action.payload = mockResponse;

        expect(recoverReducer(undefined, action)).toEqual({
            ...initialState,
            recover: {
                ...initialState.recover,
                status: mockResponse.data.status
            }
        });
    });

    it('Should handle REJECTED PASSWORD RECOVER with response 404', () => {
        const mockResponse = {
            response: {
                status: 404
            }
        };
        action.type = PASSWORD_RECOVER + '_REJECTED';
        action.payload = mockResponse;
        expect(recoverReducer(undefined, action)).toEqual({
            ...initialState,
            recover: {
                ...initialState.recover,
                status: 'error',
                errorMessage: 'No user exists with the given email'
            }
        });

    });

    it('Should handle PASSWORD RESET status ACCEPTED', () => {
        const mockResponse = {
            data: {
                status: 'accepted',
                token: 'user-token'
            }
        };
        action.type = PASSWORD_RESET + '_FULFILLED';
        action.payload = mockResponse;

        expect(recoverReducer(undefined, action)).toEqual({
            ...initialState,
            reset: {
                ...initialState.reset,
                status: mockResponse.data.status
            }
        });
    });

    it('Should handle REJECTED PASSWORD RESET with response 400', () => {
        const mockResponse = {
            response: {
                status: 400
            }
        };
        action.type = PASSWORD_RESET + '_REJECTED';
        action.payload = mockResponse;
        expect(recoverReducer(undefined, action)).toEqual({
            ...initialState,
            reset: {
                ...initialState.reset,
                status: 'error',
                errorMessage: 'The token is not valid'
            }
        });

    });
});
