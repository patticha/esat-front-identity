import {
    STATUS_ACCEPTED,
    STATUS_ERROR,
    ERRORS,
    DETAULT_TERMS
} from '../../Utils/constants';
import {
    USER_REGISTER,
    GET_LATEST_TERMS
} from '../ActionTypes';
import { Mixpanel } from '../../Helpers/Mixpanel';

const initialState = {
    terms: {
        version: undefined,
        url: DETAULT_TERMS
    },
    status: undefined,
    errorMessage: undefined
};

export default function(state = initialState, action) {
    switch (action.type) {
        case USER_REGISTER + '_FULFILLED': {
            Mixpanel.track('[FORM] Registration Accepted');
            return {
                ...state,
                status: STATUS_ACCEPTED
            };
        }

        case USER_REGISTER + '_REJECTED': {
            let { response } = action.payload;
            Mixpanel.track(`[FORM] Registration Failed ${response?.status}`);

            switch (response?.status) {
                case 409: {
                    return {
                        ...state,
                        status: STATUS_ERROR,
                        errorMessage: ERRORS.REGISTER[409]
                    };
                }

                default:
                    return state;
            }

        }

        case GET_LATEST_TERMS + '_FULFILLED': {
            const { data } = action.payload;

            return {
                ...state,
                terms: {
                    version: data?.version,
                    url: data?.url
                }

            };
        }

        default:
            return state;
    }
}
