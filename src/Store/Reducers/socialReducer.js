import {
    STATUS_ACCEPTED,
    STATUS_ERROR
} from '../../Utils/constants';
import {
    FACEBOOK_CONNECT,
    GOOGLE_CONNECT
} from '../ActionTypes';

export const initialState = {
    user: {},
    status: undefined,
    errorMessage: undefined
};

export default function(state = initialState, action) {
    switch (action.type) {
        case FACEBOOK_CONNECT + '_FULFILLED': {
            const { data } = action?.payload;
            return {
                ...state,
                user: data,
                status: STATUS_ACCEPTED
            };
        }

        case FACEBOOK_CONNECT + '_REJECTED': {
            return {
                ...state,
                status: STATUS_ERROR
            };
        }

        case GOOGLE_CONNECT + '_FULFILLED': {
            const { data } = action?.payload;
            return {
                ...state,
                user: data,
                status: STATUS_ACCEPTED
            };
        }

        case GOOGLE_CONNECT + '_REJECTED': {
            return {
                ...state,
                status: STATUS_ERROR
            };
        }

        default:
            return state;
    }
}
