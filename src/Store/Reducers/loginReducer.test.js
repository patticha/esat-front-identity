import {
    APP_CLEAR_STORE,
    USER_LOGIN,
    APP_GET_IP_GEO
} from '../ActionTypes';

import loginReducer from './loginReducer';

let action;
beforeEach(() => {
    action = {
        type: undefined,
        payload: {}
    };
});

const initialState = {
    user: {
        locale: navigator.language,
        // eslint-disable-next-line new-cap
        timeZone: Intl?.DateTimeFormat?.().resolvedOptions?.().timeZone,
        twoFactor: false,
        isAuthenticated: false
    },
    status: undefined,
    errorMessage: undefined
};
describe('[Store] LoginReducer:', () => {
    it('Should return initial state', () => {
        expect(loginReducer(undefined, {})).toEqual(initialState);
    });

    it('Should return default state for unhandled status LOGIN', () => {
        action.type = USER_LOGIN + '_FULFILLED';
        const mockResponse = {
            data: {
                status: 'doesnexist',
                token: 'user-token'
            }
        };
        action.payload = mockResponse;

        expect(loginReducer(undefined, action)).toEqual(initialState);
    });

    it('Should handle LOGIN status PENDING', () => {
        const mockResponse = {
            data: {
                status: 'pending',
                token: 'user-token'
            }
        };
        action.type = USER_LOGIN + '_FULFILLED';
        action.payload = mockResponse;
        expect(loginReducer(undefined, action)).toEqual({
            ...initialState,
            status: mockResponse.data.status,
            user: {
                twoFactor: true,
                token: mockResponse.data.token
            }
        });
    });

    it('Should handle LOGIN status ACCEPTED', () => {
        const mockResponse = {
            data: {
                status: 'accepted',
                token: 'user-token'
            }
        };
        action.type = USER_LOGIN + '_FULFILLED';
        action.payload = mockResponse;
        expect(loginReducer(undefined, action)).toEqual({
            ...initialState,
            status: mockResponse.data.status,
            user: {
                twoFactor: false,
                isAuthenticated: true,
                token: mockResponse.data.token
            }
        });
    });

    it('Should return default state for unhandled REJECTED LOGIN', () => {
        const mockResponse = {
            response: {
                status: 'doesnexist'
            }
        };
        action.type = USER_LOGIN + '_REJECTED';
        action.payload = mockResponse;
        expect(loginReducer(undefined, action)).toEqual({
            ...initialState
        });
    });

    it('Should handle REJECTED LOGIN with response 401 ', () => {
        const mockResponse = {
            response: {
                status: 401
            }
        };
        action.type = USER_LOGIN + '_REJECTED';
        action.payload = mockResponse;
        expect(loginReducer(undefined, action)).toEqual({
            ...initialState,
            status: 'error',
            errorMessage: 'Wrong password'

        });

        let state = {
            ...initialState,
            user: {
                ...initialState.user,
                twoFactor: true
            }
        };
        expect(loginReducer(state, action)).toEqual({
            status: 'error',
            errorMessage: 'Wrong verification code or password',
            user: {
                ...initialState.user,
                twoFactor: false
            }

        });
    });

    it('Should handle REJECTED LOGIN with response 404 ', () => {
        const mockResponse = {
            response: {
                status: 404
            }
        };
        action.type = USER_LOGIN + '_REJECTED';
        action.payload = mockResponse;
        expect(loginReducer(undefined, action)).toEqual({
            ...initialState,
            status: 'error',
            errorMessage: 'This email does not match any account'

        });
    });

    it('Should clear store', () => {
        action.type = APP_CLEAR_STORE;
        expect(loginReducer(undefined, action)).toEqual({
            ...initialState,
            user: {
                ...initialState.user
            }
        });
    });

    it('Should get APP_GET_IP_GEO', () => {
        const mockResponse = {
            languages: 'en, gr',
            time_zone: {
                name: 'Athens/Greece'
            }
        };
        action.type = APP_GET_IP_GEO + '_FULFILLED';
        action.payload = mockResponse;

        expect(loginReducer(undefined, action)).toEqual({
            ...initialState,
            user: {
                ...initialState.user,
                locale: 'en',
                loading: false,
                timeZone: 'Athens/Greece'
            }
        });
    });

    it('Should return dafault state in case APP_GET_IP_GEO faills to return the expected data', () => {
        const mockResponse = {
            languages: '',
            time_zone: {
                name: ''
            }
        };
        action.type = APP_GET_IP_GEO + '_FULFILLED';
        action.payload = mockResponse;

        expect(loginReducer(undefined, action)).toEqual({
            ...initialState,
            user: {
                ...initialState.user,
                loading: false
            }
        });
    });
});
