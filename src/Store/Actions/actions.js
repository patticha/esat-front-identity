import {
    USER_LOGIN,
    USER_REGISTER,
    GET_LATEST_TERMS,
    PASSWORD_RECOVER,
    PASSWORD_RESET,
    FACEBOOK_CONNECT,
    GOOGLE_CONNECT,
    APP_CLEAR_STORE,
    APP_GET_IP_GEO
} from '../ActionTypes';
import { Auth, Sys } from '../../Helpers/Client';
import { IP_GEOLOCATION_URL, IP_GEOLOCATION_KEY } from '../../Utils/constants';

export const userLogin = (email, password, token, captcha) => ({
    type: USER_LOGIN,
    payload: Auth.loginPost({ email, password, token, captcha })
});

// eslint-disable-next-line camelcase
export const passwordReset = (email, token, password, repeatPassword, captcha) => ({
    type: PASSWORD_RESET,
    payload: Auth.passwordResetPost({ email, token, password, repeat_password: repeatPassword, captcha })
});

export const passwordRecover = (email, captcha) => ({
    type: PASSWORD_RECOVER,
    payload: Auth.passwordRecoverPost({ email, captcha })
});

export const userRegister = (fullName, email, password, organizationName, locale, timeZone, termsVersion, captcha) => ({
    type: USER_REGISTER,
    payload: Auth.registerPost({
        full_name: fullName,
        email,
        password,
        organization_name: organizationName,
        locale,
        time_zone: timeZone,
        terms_version: termsVersion,
        captcha
    })
});

export const getLatestTerms = () => ({
    type: GET_LATEST_TERMS,
    payload: Sys.termsLatestGet()
});

export const facebookConnect = (state) => ({
    type: FACEBOOK_CONNECT,
    payload: Auth.oauthFacebookPost(state)
});

export const googleConnect = (state) => ({
    type: GOOGLE_CONNECT,
    payload: Auth.oauthGooglePost(state)
});

export const clearStore = () => ({
    type: APP_CLEAR_STORE,
    payload: {}
});

export const getIPGeo = () => ({
    type: APP_GET_IP_GEO,
    payload: fetch(`${IP_GEOLOCATION_URL}?apiKey=${IP_GEOLOCATION_KEY}`).then(response => response.json())
});

