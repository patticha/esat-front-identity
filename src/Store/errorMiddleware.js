
function isPromise(obj) {
    return !!obj && (typeof obj === 'object' || typeof obj === 'function') && typeof obj.then === 'function';
}

export default function errorMiddleware() {

    return next => (action) => {

        // If not a promise, continue on
        if (!isPromise(action.payload)) {
            return next(action);
        }

        // Dispatch initial pending promise, but catch any errors
        return next(action).catch(error => {
            if (error?.response?.status === 400 || error?.response?.status === 500) {
                return error;
            }
        });

    };
}
