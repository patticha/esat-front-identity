import { createStore, applyMiddleware, compose, combineReducers } from 'redux';
import promise from 'redux-promise-middleware';
import errorMiddleware from './errorMiddleware';
import loginReducer from './Reducers/loginReducer';
import registerReducer from './Reducers/registerReducer';
import recoverReducer from './Reducers/recoverReducer';
import socialReducer from './Reducers/socialReducer';

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
export default createStore(
    combineReducers(
        {
            login: loginReducer,
            register: registerReducer,
            recover: recoverReducer,
            social: socialReducer
        }
    ),
    composeEnhancers(
        applyMiddleware(
            errorMiddleware,
            promise
        )
    )
);
