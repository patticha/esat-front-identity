/**
 * Check if the field passes the validations
 *
 * @param {Object} validations - Object with the validations to be checked
 * @param {String} name - Name of the field
 * @param {String} value - Value of the field
 * @returns {String|undefined} - Message if the validation fails
 */
const validateField = (validations, name, value) => {
    const rules = validations?.[name];

    if (rules && rules.length) {

        // if the pattern rule is registered
        for (const rule of rules) {
            if (rule.pattern) {
                if (!new RegExp(rule.pattern.value).exec(value)) {
                    return rule.pattern.message || 'invalid';
                }
            }

            if (rule.validate && typeof rule.validate === 'function') {
                const error = rule.validate(value);

                if (error) {
                    return error;
                }
            }
        }
    }
};

export default validateField;
