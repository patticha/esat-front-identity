import { _isEmpty } from './misc';

describe('Misc Helper', () => {
    test('_isEmpty should return true', () => {
        let emptyObject = { first: undefined, second: undefined };
        expect(_isEmpty(emptyObject)).toBeTruthy();
    });

    test('_isEmpty should return false', () => {
        let emptyObject = { first: undefined, second: 'undefined' };
        expect(_isEmpty(emptyObject)).toBeFalsy();
    });
});
