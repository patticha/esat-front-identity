import validateField from './validateField';
import { patterns } from './validatorPatterns';

let validations = {
    email: [patterns.email, patterns.required],
    require: [patterns.required],
    checkbox: [patterns.checkbox],
    eightDigits: [patterns.eightDigits]
};

it('Should return if validation name does not exist', () => {
    expect(validateField(validations, 'not-valid', 'test@test.com')).toBe(undefined);
});

it('Should validate RegEx email rule', () => {
    expect(validateField(validations, 'email', 'invald')).toBe('This is not a valid email address');
    expect(validateField(validations, 'email', 'test@test.com')).toBe(undefined);
});

it('Should validate RegEx required rule', () => {
    expect(validateField(validations, 'require', ' ')).toBe('This field is required');
    expect(validateField(validations, 'require', 'something')).toBe(undefined);
});

it('Should validate Function checkbox rule', () => {
    expect(validateField(validations, 'checkbox', true)).toBe(undefined);
    expect(validateField(validations, 'checkbox', false)).toBe('Checkbox is required');
});

it('Should validate RegEx eight digits rule', () => {
    expect(validateField(validations, 'eightDigits', '12345678')).toBe(undefined);
    expect(validateField(validations, 'eightDigits', 'asdfg1224')).toBe('Please enter 8 digits number');
});

