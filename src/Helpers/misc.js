/**
 * Check if the object is empty
 * @param {Object} object
 * @returns boolean
 */
export const _isEmpty = (object) => {

    for (const property in object) {
        if (object[property] !== undefined || object[property] === '') {
            return false;
        }
    }

    return true;
};
