import { AuthenticationApi }  from '../../packages/auth-api/dist/index';
import { SystemApi }  from '../../packages/sys-api/dist/index.js';
export const Auth = new AuthenticationApi(undefined, BASE_URL + '/@auth');
export const Sys = new SystemApi(undefined, BASE_URL + '/@sys');
