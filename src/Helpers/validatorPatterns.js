import intl from '../Utils/createIntl';

export const patterns = {
    required: {
        pattern: {
            value: '[a-zA-Z0-9-,]+',
            message: intl.formatMessage({ defaultMessage: 'This field is required' })
        }
    },
    email: {
        pattern: {
            value: '[^@]+@[^.]+..+',
            message: intl.formatMessage({ defaultMessage: 'This is not a valid email address' })
        }
    },
    checkbox: {
        validate: (value) => !value ? intl.formatMessage({ defaultMessage: 'Checkbox is required' }) : null
    },
    eightDigits: {
        pattern: {
            value: '^[0-9]{8}$',
            message: intl.formatMessage({ defaultMessage: 'Please enter 8 digits number' })
        }
    }
};

