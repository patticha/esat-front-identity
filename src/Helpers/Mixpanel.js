import mixpanel from 'mixpanel-browser';
import { MIXPANEL_ID } from '../Utils/constants';
mixpanel.init(MIXPANEL_ID);

let prod =  process.env.NODE_ENV === 'production';

const actions = {
    identify: (id) => {
        if (prod) {mixpanel.identify(id);}
    },
    alias: (id) => {
        if (prod) {mixpanel.alias(id);}
    },
    track: (name, props) => {
        if (prod) {mixpanel.track(name, props);}
    }
};

export const Mixpanel = actions;
