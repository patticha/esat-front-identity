import { useLocation } from 'react-router-dom';

export const useParams = () => new URLSearchParams(useLocation().search);

export const redirect = (url) => location.replace(url);
