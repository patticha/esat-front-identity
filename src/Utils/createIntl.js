import { createIntl, createIntlCache } from 'react-intl';

/**
 * This allows you to format things outside of React lifecycle
 * while reusing the same intl object.
*/
const cache = createIntlCache();

const intl = createIntl(
    {
        locale: 'en'
    }
    , cache
);

export default intl;
