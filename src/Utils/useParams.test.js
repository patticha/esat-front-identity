import { useParams, redirect } from './useParams';

jest.mock('react-router-dom', () => {

    return {
        useLocation: () => {
            return {
                search: 'token=value'
            };
        }
    };
});

it('Should redirect the page', () => {
    delete window.location;
    window.location = { replace: jest.fn() };

    const page = 'https://www.another.com';
    redirect(page);
    expect(window.location.replace).toHaveBeenCalled();
});

it('Should get the URL params', () => {
    const params =  useParams();
    expect(params.get('token')).toBe('value');
});
