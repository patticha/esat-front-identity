import React, { useState } from 'react';
import Proptypes from 'prop-types';
import validateField from '../Helpers/validateField';
import { ReCaptchaV3 } from 'react-recaptcha-x';
import { Mixpanel } from '../Helpers/Mixpanel';

export const useForm = (initialValues, validations) => {
    const [values, setValues] = useState(initialValues);
    const [errors, setErrors] = useState({});

    const handleInputChange = event => {
        let { name, value, checked } = event.target;

        if (checked) {
            value = checked;
        }

        setValues({
            ...values,
            [name]: value
        });

        setErrors({
            ...errors,
            [name]: validateField(validations, name, value)
        });

    };

    const handleSubmitForm = (event) => {
        event.preventDefault();
        const form = new FormData(event.target);
        const mxevent = event?.target?.getAttributeNode('mxevent')?.value;

        if (mxevent) {
            Mixpanel.track(`[FORM] ${mxevent}`);
        }

        return form;
    };

    const isValid = () => !Object.keys(validations).some(name => validateField(validations, name, values[name]));

    return [values, errors, isValid, handleInputChange, handleSubmitForm];
};

export function Form({ children, captcha = 'form', ...props }) {
    const [token, setToken] = useState();

    const captchaCallback = (token) =>  {
        if (typeof token === 'string') {
            setToken(token);
        }
    };

    return (
        <form  method="POST" autoComplete="off" {...props}>
            {children}
            <input hidden name="captcha" value={token} />
            <ReCaptchaV3 action={captcha} callback={captchaCallback} />
        </form>
    );
}

Form.propTypes = {
    children: Proptypes.any,
    captcha: Proptypes.string
};
