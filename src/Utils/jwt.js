import Cookies from 'js-cookie';

/*** Login/Logout ***/
export function login(token, rememberMe) {
    let attributes = {
        domain: DOMAIN
    };

    if (rememberMe) {
        attributes = {
            expires: 365
        };
    }

    Cookies.set(
        'esat_user',
        token,
        attributes
    );
}

export function loggedIn() {
    return !!(Cookies.get('esat_user'));
}
