import { login } from './jwt';
import Cookies from 'js-cookie';

test('Set Cookie with token', () => {
    const mockSet = jest.fn(attr => attr);
    Cookies.set = mockSet;
    login('token', false);
    expect(mockSet).toHaveBeenCalledWith('esat_user', 'token', { domain: '.e-satisfaction.com' });
});

test('Set Cookie to expire in a year', () => {
    const mockSet = jest.fn(attr => attr);
    Cookies.set = mockSet;
    login('token', true);
    let options =  { expires: 365 };
    expect(mockSet).toHaveBeenCalledWith('esat_user', 'token', options);
});
