import React, { Suspense, lazy, useEffect } from 'react';
import PropTypes from 'prop-types';
import { useIntl } from 'react-intl';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import { Mixpanel } from '../Helpers/Mixpanel';
import { loggedIn } from '../Utils/jwt';
import { useParams, redirect } from '../Utils/useParams';
import { DASHBOARD_URL } from '../Utils/constants';

const LoginPage = lazy(() =>
    import(/* webpackChunkName: "Login" */ '../Components/SmartComponents/Login/Login'));
const RegisterPage = lazy(() =>
    import(/* webpackChunkName: "Register" */ '../Components/SmartComponents/Register/Register'));
const ResetPasswordPage = lazy(() =>
    import(/* webpackChunkName: "ResetPassword" */ '../Components/SmartComponents/ResetPassword/ResetPassword'));
const RecoverPasswordPage = lazy(() =>
    import(/* webpackChunkName: "RecoverPassword" */ '../Components/SmartComponents/RecoverPassword/RecoverPassword'));

const Page = ({ component: Component, rootClass = 'esat-id', title = 'Login', mxevent, ...rest }) => {
    const intl = useIntl();
    const URL = useParams();;
    const root = document.getElementById('container');
    root?.classList.add(`page__${rootClass}`);

    useEffect(() => {
        if (loggedIn()) {
            redirect(URL.get('next') || DASHBOARD_URL);
        }
    }, []);

    if (title) {
        document.title = intl.formatMessage(
            { defaultMessage: '{title} | e-satisfaction.com' },
            { title }
        );
    }

    if (mxevent) {
        Mixpanel.track(`[PAGE LOAD] ${mxevent}`);
    }

    return <Route {...rest} component={Component} />;
};

Page.propTypes = {
    component: PropTypes.func,
    rootClass: PropTypes.string,
    title: PropTypes.string,
    mxevent: PropTypes.string
};
export const Routes = () => {
    return (
        <Router>
            <Suspense fallback={<div>Loading...</div>}>
                <Switch>
                    <Page
                        mxevent='Registration'
                        title='Register'
                        path='/register'
                        component={RegisterPage}
                    />
                    <Page
                        mxevent='Login'
                        title='Login'
                        path='/login'
                        component={LoginPage}
                    />
                    <Page
                        mxevent='Password Reset'
                        title='Reset Password'
                        path='/password/reset'
                        component={ResetPasswordPage}
                    />
                    <Page
                        mxevent='Password Recovery'
                        title='Recover Password'
                        path='/password/recover'
                        component={RecoverPasswordPage}
                    />
                    <Page component={LoginPage} />
                </Switch>
            </Suspense>
        </Router>
    );
};

