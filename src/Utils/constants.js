import intl from '../Utils/createIntl';

export const FB_ID = '760150754827812';
export const CAPTCHA_SITE_KEY = '6LcTDNMZAAAAAHsAi6lrmIeSa2M13CDZerUw6V0m';
export const GOOGLE_ID = '255152359089-ofotig9ldmbnu5vfg8njrtgjs1ra87tf.apps.googleusercontent.com';
export const MIXPANEL_ID = '757a52a871954a9f562f1008087fa9cb';
export const INTERCOM_ID = 'epasg8sg';
export const IP_GEOLOCATION_KEY = 'b62ab05638684a26bc0450ebb9eb1ddb';
export const IP_GEOLOCATION_URL = 'https://api.ipgeolocation.io/ipgeo';

export const DETAULT_TERMS = 'https://www.e-satisfaction.com/home/terms-conditions/';
export const DEFAULT_LOCALE = 'en-US';
export const DASHBOARD_URL = 'https://app.e-satisfaction.com';

export const STATUS_PENDING = 'pending';
export const STATUS_ACCEPTED = 'accepted';
export const STATUS_ERROR = 'error';

export const ERRORS = {
    LOGIN: {
        404: intl.formatMessage({
            defaultMessage: 'This email does not match any account'
        }),
        401: intl.formatMessage({
            defaultMessage: 'Wrong password'
        })
    },
    LOGIN_2_FACTOR: {
        401: intl.formatMessage({
            defaultMessage: 'Wrong verification code or password'
        })
    },
    REGISTER: {
        409: intl.formatMessage({
            defaultMessage: 'User already exists with the same email'
        })
    },
    RECOVER: {
        404: intl.formatMessage({
            defaultMessage: 'No user exists with the given email'
        })
    },
    RESET: {
        400: intl.formatMessage({
            defaultMessage: 'The token is not valid'
        })
    }
};
