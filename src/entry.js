import React, { useEffect } from 'react';
import ReactDOM from 'react-dom';
import { useDispatch } from 'react-redux';
import { getIPGeo } from './Store/Actions/actions';

import App from './App';
import { ReCaptchaProvider } from 'react-recaptcha-x';
import { CAPTCHA_SITE_KEY } from './Utils/constants';

import { Provider } from 'react-redux';
import store from './Store/index';

const MainWrapper = () => {
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(getIPGeo());
    }, []);

    return <App />;
};

const MainApp = () => {

    return (
        <ReCaptchaProvider
            siteKeyV3={CAPTCHA_SITE_KEY}
            langCode="en"
        >
            <Provider store={store}>
                <MainWrapper />
            </Provider>
        </ReCaptchaProvider>
    );
};

ReactDOM.render(
    <MainApp/>,
    document.getElementById('container')
);
