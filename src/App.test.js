import App from './App';
import configureStore from 'redux-mock-store';
import { Provider } from 'react-redux';
import { BrowserRouter as Router } from 'react-router-dom';
import { ReCaptchaProvider } from 'react-recaptcha-x';
import { CAPTCHA_SITE_KEY } from './Utils/constants';

jest.mock('react-redux', () => {
    const { Provider, useSelector } = jest.requireActual('react-redux');

    return {
        useDispatch: jest.fn(),
        useSelector,
        Provider
    };
});

let mockSocialStore = {
    login: {
        user: {
            token: 'mytoken'
        }
    },
    social: {
        user: {
            token: 'mytoken'
        }
    }
};

const mockStore = configureStore(mockSocialStore);

const mountComponent = (store, Comp) => {
    return mountWithIntl(
        <ReCaptchaProvider
            siteKeyV3={CAPTCHA_SITE_KEY}
            langCode="en">
            <Provider store={store}>
                <Router>
                    <Comp />
                </Router>
            </Provider>
        </ReCaptchaProvider>
    );
};

it('Should render App with el-GR', () => {
    mockSocialStore.login.user.locale = 'el-GR';
    const tempStore = mockStore(mockSocialStore);
    const wrapper = mountComponent(tempStore, App);
    const locale = wrapper.find('IntlProvider').props().locale;
    const messages = wrapper.find('IntlProvider').props().messages;
    expect(messages.AyGauy).toBe('Σύνδεση');
    expect(locale).toBe('el-GR');
});

it('Should render App with en-US', () => {
    mockSocialStore.login.user.locale = 'en-US';
    const tempStore = mockStore(mockSocialStore);
    const wrapper = mountComponent(tempStore, App);
    const locale = wrapper.find('IntlProvider').props().locale;
    const messages = wrapper.find('IntlProvider').props().messages;
    expect(messages.AyGauy).toBe('Login');
    expect(locale).toBe('en-US');
});

it('Should render App with en-GB', () => {
    mockSocialStore.login.user.locale = 'en-GB';
    const tempStore = mockStore(mockSocialStore);
    const wrapper = mountComponent(tempStore, App);
    const locale = wrapper.find('IntlProvider').props().locale;
    const messages = wrapper.find('IntlProvider').props().messages;
    expect(messages.AyGauy).toBe('Login');
    expect(locale).toBe('en-GB');
});

it('Should render App with default locale', () => {
    mockSocialStore.login.user.locale = 'doesnt-exist';
    const tempStore = mockStore(mockSocialStore);
    const wrapper = mountComponent(tempStore, App);
    const locale = wrapper.find('IntlProvider').props().locale;
    const messages = wrapper.find('IntlProvider').props().messages;
    expect(messages.AyGauy).toBe('Login');
    expect(locale).toBe('doesnt-exist');
});

