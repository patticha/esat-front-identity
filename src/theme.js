import { createMuiTheme, responsiveFontSizes } from '@material-ui/core/styles';
import MontserratTTF from '../assets/fonts/Montserrat-Regular.ttf';

const Montserrat = {
    fontFamily: 'Montserrat',
    fontStyle: 'normal',
    fontDisplay: 'swap',
    fontWeight: 400,
    src: ` local('Montserrat-Regular'),
    url(${MontserratTTF}) format('ttf')`
};

let theme = createMuiTheme({
    typography: {
        fontFamily: [
            'Montserrat, Roboto'
        ].join(','),
        button: {
            textTransform: 'none'
        }
    },
    overrides: {
        MuiCssBaseline: {
            '@global': {
                '@font-face': [Montserrat]
            }
        }
    },
    palette: {
        primary: {
            main: '#1976D2'
        }
    }
});

theme = responsiveFontSizes(theme);
export default theme;
