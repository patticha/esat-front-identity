/* eslint-disable camelcase */
import React from 'react';
import MainWrapper from './Components/Presentational/MainWrapper/MainWrapper';
import { useSelector } from 'react-redux';
import { IntlProvider } from 'react-intl';
import { Routes } from './Utils/Routes';
import { ThemeProvider } from '@material-ui/core/styles';
import { SnackbarProvider } from 'notistack';
import en_US from '../lang/locales/en-US.json';
import el_GR from '../lang/locales/el-GR.json';

import theme from './theme';
import './Helpers/Intercom';
import './app.scss';

function loadLocaleData(locale) {
    switch (locale) {
        case 'en-US':
            return en_US;
        case 'en-GB':
            return en_US;
        case 'el-GR':
            return el_GR;
        default:
            return en_US;
    }
}

const App = () => {
    const {
        user
    } = useSelector(state => state.login);

    return (
        !user.loading && <IntlProvider locale={user.locale} messages={loadLocaleData(user.locale)}>
            <ThemeProvider theme={theme}>
                <SnackbarProvider
                    hideIconVariant
                    dense
                    maxSnack={3}
                    anchorOrigin={{
                        vertical: 'top',
                        horizontal: 'center'
                    }}
                >
                    <MainWrapper>
                        <Routes/>
                    </MainWrapper>
                </SnackbarProvider>
            </ThemeProvider>
        </IntlProvider>
    );
};

export default App;
