import React from 'react';
import PropTypes from 'prop-types';
import googleIcon from '../../../../assets/icons/googleIcon.svg';
import facebookIcon from '../../../../assets/icons/facebookIcon.svg';

export const GoogleIcon = ({ props }) =>  {
    return (
        <img src={googleIcon} alt="Google Logo" {...props} />
    );
};

GoogleIcon.propTypes = {
    props: PropTypes.object
};

export const FacebookIcon = ({ props }) =>  {
    return (
        <img src={facebookIcon} alt="Faceboo Logo" {...props} />
    );
};

FacebookIcon.propTypes = {
    props: PropTypes.object
};
