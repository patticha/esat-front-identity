import { GoogleIcon, FacebookIcon } from './CustomIcons';

test('Should render google icon', () => {
    const wrapper = mount(<GoogleIcon />);
    expect(wrapper.find('GoogleIcon img').prop('src')).toBe('test-asset');
});

test('Should render facebookg icon', () => {
    const wrapper = mount(<FacebookIcon />);
    expect(wrapper.find('FacebookIcon img').prop('src')).toBe('test-asset');
});
