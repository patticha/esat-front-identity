import FormMessage from './FormMessage';

it('Should render FormMessage', () => {
    const wrapper = mountWithIntl(
        <FormMessage message="my message"/>
    );

    expect(toJson(wrapper)).toMatchSnapshot();
});

it('Should render FormMessage with given message', () => {
    const wrapper = mountWithIntl(
        <FormMessage message="my message"/>
    );

    expect(wrapper.find('p').text()).toBe('my message');
});

it('Should render FormMessage with an error', () => {
    const wrapper = mountWithIntl(
        <FormMessage error message="my message"/>
    );

    expect(wrapper.find('svg')).toHaveLength(1);
});

