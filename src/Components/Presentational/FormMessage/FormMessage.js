import React, { Fragment } from 'react';
import Proptypes from 'prop-types';
import Typography from '@material-ui/core/Typography';
import ErrorIcon from '@material-ui/icons/Error';
import Box from '@material-ui/core/Box';

const FormMessage = ({ message, variant, color, error, ...props }) => {
    return <Fragment>
        {
            message &&
            <Box display="flex" mt={1}>
                {error &&
                    <Box ml={1} mr={1}>
                        <ErrorIcon color={'error'} fontSize="small" />
                    </Box>
                }
                <Box>
                    <Typography color={color} display="inline" variant={variant} {...props} gutterBottom>
                        {message}
                    </Typography>
                </Box>
            </Box>
        }
    </Fragment>;
};

FormMessage.defaultProps = {
    variant: 'body2',
    color: 'error'
};

FormMessage.propTypes = {
    message: Proptypes.string,
    variant: Proptypes.string,
    color: Proptypes.string,
    error: Proptypes.bool
};

export default FormMessage;
