import FormFooter from './FormFooter';

it('Should render FormFooter with children ', () => {
    const wrapper = mountWithIntl(
        <FormFooter>
            <span> This is the test </span>
            <a>This is the link</a>
        </FormFooter>
    );

    expect(toJson(wrapper)).toMatchSnapshot();
});

