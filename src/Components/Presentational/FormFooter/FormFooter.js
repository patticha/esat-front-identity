import React from 'react';
import PropTypes from 'prop-types';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';

const FormFooter = ({ children: [text, link, ...rest],  ...props }) => (
    <Typography align="center" variant="subtitle1">
        <Box fontWeight={500} mt={3} {...props}>
            {text}
            <Box component={'span'} fontWeight={700}>
                {link}
            </Box>
            {rest}
        </Box>
    </Typography>
);

FormFooter.propTypes = {
    children: PropTypes.element
};
export default FormFooter;
