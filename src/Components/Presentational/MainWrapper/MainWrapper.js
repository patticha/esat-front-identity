import React from 'react';
import { useIntl } from 'react-intl';
import PropTypes from 'prop-types';
import CssBaseline from '@material-ui/core/CssBaseline';
import Grid from '@material-ui/core/Grid';
import Container from '@material-ui/core/Container';
import useStyles from './MainWrapper.style';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';

const MainWrapper = ({ children }) => {
    const intl = useIntl();
    const classes = useStyles();
    return (
        <Grid container component="main" className={classes.root}>
            <CssBaseline />
            <Grid item xs={6} elevation={6} square>
                <div className={classes.logo}/>
                <Container component="main" maxWidth="xs">
                    {children}
                </Container>
            </Grid>
            <Grid item xs={6} className={classes.image}>
                <Typography component="h3" variant="h3">
                    <Box maxWidth={550} fontWeight={700} mt={10} mb={2} mx={10}>
                        {intl.formatMessage(
                            {
                                defaultMessage: 'Know who your customers really are!'
                            }
                        )}
                    </Box>
                </Typography>
                <Typography component="h6" variant="h6">
                    <Box maxWidth={450} lineHeight={'1.3'} fontWeight={700} mx={10}>
                        {intl.formatMessage(
                            {
                                defaultMessage: 'Learn what your customer think of you and your services'
                            }
                        )}
                    </Box>
                </Typography>
            </Grid>
        </Grid>
    );
};

MainWrapper.propTypes = {
    children: PropTypes.element
};

export default MainWrapper;

