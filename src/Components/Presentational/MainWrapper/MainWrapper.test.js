import MainWrapper from './MainWrapper';

it('Should render MainWrapper', () => {
    const wrapper = mountWithIntl(
        <MainWrapper />
    );

    expect(toJson(wrapper)).toMatchSnapshot();
});

