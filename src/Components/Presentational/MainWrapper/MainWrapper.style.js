import { makeStyles } from '@material-ui/core/styles';
import loginSideImg from '../../../../assets/images/login-side-image.jpg';
import whiteLogo from '../../../../assets/images/logo-white.svg';

const useStyles = makeStyles((theme) => ({
    root: {
        height: '100vh'
    },
    image: {
        backgroundImage: `url(${loginSideImg})`,
        backgroundRepeat: 'no-repeat',
        backgroundSize: 'cover',
        backgroundPosition: 'center',
        color: '#fff'
    },
    logo: {
        backgroundImage: `url(${whiteLogo})`,
        backgroundRepeat: 'no-repeat',
        backgroundSize: 'cover',
        backgroundPosition: 'center',
        width: '200px',
        height: '80px',
        margin: theme.spacing(4, 4)
    },
    content: {
        margin: theme.spacing(8, 4),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center'
    }
}));

export default useStyles;
