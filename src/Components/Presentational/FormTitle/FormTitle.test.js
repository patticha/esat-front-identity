import FormTitle from './FormTitle';

it('Should render FormTitle', () => {
    const wrapper = mountWithIntl(
        <FormTitle />
    );

    expect(toJson(wrapper)).toMatchSnapshot();
});

it('Should render FormMessage with given titlt', () => {
    const wrapper = mountWithIntl(
        <FormTitle title="my title"/>
    );

    expect(wrapper.find('h1').text()).toBe('my title');
});

it('Should render FormMessage with an subtitle', () => {
    const wrapper = mountWithIntl(
        <FormTitle subTitle="subtitle" message="my message"/>
    );

    expect(wrapper.find('p').text()).toBe('subtitle');
});

