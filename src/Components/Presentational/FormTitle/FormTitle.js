import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';

const FormTitle = ({ title, subTitle,  ...props }) => (
    <Fragment>
        <Typography component="h1" color="primary" variant="h5">
            <Box fontWeight={700} mb={3} {...props}>
                {title}
            </Box>

        </Typography>

        {subTitle &&
         <Typography component="p" variant="subtitle2">
             <Box fontWeight={400} mt={3} mb={3} {...props}>
                 {subTitle}
             </Box>
         </Typography>
        }
    </Fragment>
);

FormTitle.propTypes = {
    title: PropTypes.string,
    subTitle: PropTypes.string
};
export default FormTitle;
