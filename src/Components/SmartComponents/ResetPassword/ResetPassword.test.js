/* eslint-disable max-len */
import ResetPassword from './ResetPassword';
import { BrowserRouter as Router } from 'react-router-dom';
import { CAPTCHA_SITE_KEY } from '../../../Utils/constants';
import configureStore from 'redux-mock-store';
import { Provider } from 'react-redux';
import { SnackbarProvider } from 'notistack';
import { ReCaptchaProvider } from 'react-recaptcha-x';

jest.mock('react-redux', () => {
    const { Provider, useSelector, useDispatch } = jest.requireActual('react-redux');

    return {
        useDispatch,
        useSelector,
        Provider
    };
});

const mountComponent = (store, Comp) => {
    return mountWithIntl(
        <ReCaptchaProvider
            siteKeyV3={CAPTCHA_SITE_KEY}
            langCode="en">
            <Provider store={store}>
                <Router>
                    <SnackbarProvider>
                        <Comp />
                    </SnackbarProvider>
                </Router>
            </Provider>
        </ReCaptchaProvider>
    );
};

let mockRecoverStore = {
    recover: {
        reset: {
            status: ''
        }
    },
    social: {
        user: {
            token: 'mytoken'
        },
        status: ''
    }
};
const mockStore = configureStore(mockRecoverStore);
const tempStore = mockStore(mockRecoverStore);

beforeEach(() => {
    tempStore.clearActions();
});

describe('Recover', () => {

    test('Should render ResetPassword page', () => {
        const tempStore = mockStore(mockRecoverStore);
        const wrapper = mountComponent(tempStore, ResetPassword);
        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('ResetPassword on success should display message 8 digits', () => {
        mockRecoverStore.recover.reset.status = 'accepted';
        const tempStore = mockStore(mockRecoverStore);
        const wrapper = mountComponent(tempStore, ResetPassword);
        expect(wrapper.find('FormTitle p').text()).toBe('Enter your new desired password and the 8-digit code you received on you email below.');
    });

    test('Login should be able to show password', () => {
        const wrapper = mountComponent(tempStore, ResetPassword);
        const icon =  wrapper.find('ForwardRef(IconButton)').first();
        icon.simulate('click');
        wrapper.update();

        const passwrdField = wrapper.find('input#password');
        expect(passwrdField.prop('type')).toBe('text');
    });

    test('Login should be able to show repeat_password', () => {
        const wrapper = mountComponent(tempStore, ResetPassword);
        const icon =  wrapper.find('ForwardRef(IconButton)').at(1);
        icon.simulate('click');
        wrapper.update();
        const passwrdField = wrapper.find('input#password-repeat');
        expect(passwrdField.prop('type')).toBe('text');
    });
    test('ResetPassword should display error message', () => {
        mockRecoverStore.recover.reset.status = 'error';
        mockRecoverStore.recover.reset.errorMessage = 'This is the error';
        const tempStore = mockStore(mockRecoverStore);
        const wrapper = mountComponent(tempStore, ResetPassword);
        expect(wrapper.find('FormMessage').prop('error')).toBeTruthy();
        expect(wrapper.find('FormMessage').prop('message')).toBe('This is the error');
    });

    test('Login should submit the form', () => {
        const wrapper = mountComponent(tempStore, ResetPassword);
        let form = wrapper.find('form');
        form.simulate('submit');
        const actions = tempStore.getActions();
        const expected = {
            payload: Promise.resolve(),
            type: 'PASSWORD_RESET'
        };
        expect(actions).toEqual([expected]);
    });

});
