import React, { useState, useEffect } from 'react';
import { useIntl } from 'react-intl';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
import TextField from '@material-ui/core/TextField';
import Box from '@material-ui/core/Box';
import Button from '@material-ui/core/Button';
import FormControl from '@material-ui/core/FormControl';
import IconButton from '@material-ui/core/IconButton';
import VisibilityOutlinedIcon from '@material-ui/icons/VisibilityOutlined';
import VisibilityOffOutlinedIcon from '@material-ui/icons/VisibilityOffOutlined';
import FormTitle from '../../Presentational/FormTitle/FormTitle';
import FormMessage from '../../Presentational/FormMessage/FormMessage';
import { STATUS_ACCEPTED, STATUS_ERROR } from '../../../Utils/constants';
import { passwordReset } from '../../../Store/Actions/actions';
import { Form, useForm } from '../../../Utils/useForm';
import { patterns } from '../../../Helpers/validatorPatterns';
import { useParams } from '../../../Utils/useParams';
import { _isEmpty } from '../../../Helpers/misc';

const ResetPassword = () => {
    const [showPassword, setShowPassword] = useState(false);
    const intl = useIntl();
    const URL = useParams();
    const dispatch = useDispatch();
    let history = useHistory();

    const token = URL.get('token');
    const email = URL.get('email');

    const {
        status,
        errorMessage
    } = useSelector(state => state.recover.reset);

    let [
        values,
        errors,
        isValid,
        handleInputChange,
        handleSubmitForm
    ] = useForm(
        {
            email,
            password: '',
            repeat_password: '',
            token
        },
        {
            email: [patterns.required, patterns.email],
            password: [patterns.required],
            repeat_password: [patterns.required],
            token: [patterns.required, patterns.eightDigits]
        }
    );

    if (values?.repeat_password && values?.repeat_password !== values?.password) {
        errors = {
            repeat_password: intl.formatMessage({ defaultMessage: 'Passwords don\'n match.' }),
            password: intl.formatMessage({ defaultMessage: 'Passwords don\'n match.' })
        };
    }

    const handleSubmit = (e) => {
        const form = handleSubmitForm(e);

        dispatch(
            passwordReset(
                form.get('email'),
                form.get('token'),
                form.get('password'),
                form.get('repeat_password'),
                form.get('captcha')
            )
        );
    };

    useEffect(() => {
        if (status === STATUS_ACCEPTED) {
            history.push(`/login?email=${values.email}`);
        }

    }, [status]);

    return (
        <Form captcha="reset" mxevent='Password Reset' onSubmit={(e) => handleSubmit(e)}>
            {token
                ?  (
                    <FormTitle
                        title={intl.formatMessage({ defaultMessage: 'Password reset' })}
                        subTitle={intl.formatMessage({ defaultMessage: 'Enter your new desired password.' })}
                    />
                )
                : (
                    <FormTitle
                        title={intl.formatMessage({ defaultMessage: 'Password reset' })}
                        subTitle={intl.formatMessage(
                            {
                                // eslint-disable-next-line max-len
                                defaultMessage: 'Enter your <b>new desired password</b> and the <b>8-digit</b> code you received on you email below.'
                            },
                            {
                                b: (chunks) => <b>{chunks}</b>
                            }
                        )}
                    />
                )
            }
            <FormControl fullWidth>

                <TextField
                    value={values.email}
                    onChange={handleInputChange}
                    variant="outlined"
                    margin="normal"
                    fullWidth
                    id="email"
                    label={intl.formatMessage({ defaultMessage: 'Email' })}
                    name="email"
                    helperText={errors?.email}
                    error={!!errors?.email}
                    inputProps={{
                        readOnly: !!email
                    }}
                    InputProps={{
                        className: email && 'Mui-disabled'
                    }}
                />

                <TextField
                    value={values.password}
                    onChange={handleInputChange}
                    variant="outlined"
                    margin="normal"
                    fullWidth
                    type={showPassword ? 'text' : 'password'}
                    name="password"
                    label={intl.formatMessage({ defaultMessage: 'Enter New Password' })}
                    id="password"
                    helperText={errors?.password}
                    error={!!errors?.password}
                    InputProps={{
                        endAdornment:
                        <IconButton onClick={() => setShowPassword(!showPassword)} >
                            {showPassword ? <VisibilityOutlinedIcon /> : <VisibilityOffOutlinedIcon />}
                        </IconButton>
                    }}
                />
                <TextField
                    value={values.repeat_password}
                    onChange={handleInputChange}
                    variant="outlined"
                    margin="normal"
                    fullWidth
                    type={showPassword ? 'text' : 'password'}
                    name="repeat_password"
                    label={intl.formatMessage({ defaultMessage: 'Re-Enter New Password' })}
                    id="password-repeat"
                    helperText={errors?.repeat_password}
                    error={!!errors?.repeat_password}
                    InputProps={{
                        endAdornment:
                        <IconButton onClick={() => setShowPassword(!showPassword)} >
                            {showPassword ? <VisibilityOutlinedIcon /> : <VisibilityOffOutlinedIcon />}
                        </IconButton>
                    }}
                />
                <Box display={(token && status !== STATUS_ERROR) ? 'none' : 'block'} >
                    <TextField
                        value={values.token}
                        className
                        onChange={handleInputChange}
                        variant="outlined"
                        margin="normal"
                        fullWidth
                        id="token"
                        maxLength={8}
                        label={intl.formatMessage({ defaultMessage: '8-digit-code' })}
                        name="token"
                        helperText={errors?.token}
                        error={!!errors?.token}
                    />
                </Box>

                { status === STATUS_ERROR && <FormMessage error message={errorMessage} /> }

                <Box mt={3} mb={2}></Box>
                <Button
                    type="submit"
                    fullWidth
                    variant="contained"
                    color="primary"
                    size="large"
                    disabled={!isValid() || !_isEmpty(errors)}
                >
                    {intl.formatMessage({ defaultMessage: 'Next' })}

                </Button>
            </FormControl>
        </Form>
    );
};

export default ResetPassword ;
