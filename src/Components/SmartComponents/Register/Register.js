import React, { useEffect, useState } from 'react';
import { useIntl } from 'react-intl';
import { useDispatch, useSelector } from 'react-redux';
import Link from '@material-ui/core/Link';
import { Link as RouterLink } from 'react-router-dom';
import TextField from '@material-ui/core/TextField';
import IconButton from '@material-ui/core/IconButton';
import FormControl from '@material-ui/core/FormControl';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormHelperText from '@material-ui/core/FormHelperText';
import Checkbox from '@material-ui/core/Checkbox';
import Button from '@material-ui/core/Button';
import Box from '@material-ui/core/Box';
import VisibilityOutlinedIcon from '@material-ui/icons/VisibilityOutlined';
import VisibilityOffOutlinedIcon from '@material-ui/icons/VisibilityOffOutlined';
import FormTitle from '../../Presentational/FormTitle/FormTitle';
import FormFooter from '../../Presentational/FormFooter/FormFooter';
import FormMessage from '../../Presentational/FormMessage/FormMessage';
import SocialButtons from '../SocialButtons/SocialButtons';
import { STATUS_ACCEPTED, STATUS_ERROR } from '../../../Utils/constants';
import { Form, useForm } from '../../../Utils/useForm';
import { userRegister, getLatestTerms } from '../../../Store/Actions/actions';
import { patterns } from '../../../Helpers/validatorPatterns';
import { useParams, redirect } from '../../../Utils/useParams';

const RegisterForm = () => {
    const dispatch = useDispatch();
    const [showPassword, setShowPassword] = useState(false);
    const URL = useParams();
    const intl = useIntl();

    const {
        terms,
        status,
        errorMessage
    } = useSelector(state => state.register);

    const { user } = useSelector(state => state.login);

    const [
        values,
        errors,
        isValid,
        handleInputChange,
        handleSubmitForm
    ] = useForm(
        {
            full_name: '',
            email: URL.get('email') || '',
            password: '',
            organization_name: '',
            termCheckBox: false
        },
        {
            full_name: [patterns.required],
            email: [patterns.required, patterns.email],
            password: [patterns.required],
            organization_name: [patterns.required],
            termCheckBox: [patterns.checkbox]
        }
    );

    useEffect(() => {
        dispatch(getLatestTerms());
    }, []);

    useEffect(() => {
        if (status === STATUS_ACCEPTED) {
            redirect(URL.get('next') || '/login');
        }

    }, [status]);

    const handleSubmit = (e) => {
        const form = handleSubmitForm(e);

        dispatch(
            userRegister(
                form.get('full_name'),
                form.get('email'),
                form.get('password'),
                form.get('organization_name'),
                user.locale,
                user.timeZone,
                terms.version,
                form.get('captcha')
            )
        );
    };

    return (
        <Form captcha="register" mxevent='Registration' onSubmit={(e) => handleSubmit(e)}>
            <FormTitle title={intl.formatMessage({ defaultMessage: 'Register' })} />

            <FormControl fullWidth>
                <TextField
                    value={values.full_name}
                    onChange={handleInputChange}
                    variant="outlined"
                    margin="normal"
                    fullWidth
                    id="full name"
                    label={intl.formatMessage({ defaultMessage: 'Full Name' })}
                    name="full_name"
                    autoFocus
                    helperText={errors?.full_name}
                    error={!!errors?.full_name}
                />
                <TextField
                    value={values.email}
                    onChange={handleInputChange}
                    variant="outlined"
                    margin="normal"
                    fullWidth
                    id="email"
                    label={intl.formatMessage({ defaultMessage: 'Email' })}
                    name="email"
                    autoComplete="email"
                    helperText={errors?.email}
                    error={!!errors?.email}
                />
                <TextField
                    value={values.password}
                    onChange={handleInputChange}
                    variant="outlined"
                    margin="normal"
                    fullWidth
                    id="password"
                    label={intl.formatMessage({ defaultMessage: 'Password' })}
                    name="password"
                    type={showPassword ? 'text' : 'password'}
                    helperText={errors?.password}
                    error={!!errors?.password}
                    InputProps={{
                        endAdornment:
                        <IconButton onClick={() => setShowPassword(!showPassword)} >
                            {showPassword ? <VisibilityOutlinedIcon /> : <VisibilityOffOutlinedIcon />}
                        </IconButton>
                    }}
                />
                <TextField
                    value={values.organization_name}
                    onChange={handleInputChange}
                    variant="outlined"
                    margin="normal"
                    fullWidth
                    id="organization_name"
                    label={intl.formatMessage({ defaultMessage: 'Organization Name' })}
                    name="organization_name"
                    helperText={errors?.organization_name}
                    error={!!errors?.organization_name}
                />

                { status === STATUS_ERROR && <FormMessage error message={errorMessage} /> }

                <Box mt={3} mb={1}>
                    <FormControlLabel
                        control={
                            <Checkbox
                                checked={values.termCheckBox}
                                onChange={handleInputChange}
                                name="termCheckBox"
                                color="primary"
                            />
                        }
                        label={
                            <FormHelperText style={{ fontSize: '14px' }}>
                                { intl.formatMessage({ defaultMessage: 'I accept {space}' }, { space: '' })}

                                <Link href={terms.url} target="_blank">
                                    {intl.formatMessage({ defaultMessage: 'Terms and Condition of use' })}
                                </Link>
                            </FormHelperText>
                        }
                    />
                    {errors?.termCheckBox && <FormHelperText error>{errors?.termCheckBox}</FormHelperText> }
                </Box>
                <Button
                    type="submit"
                    fullWidth
                    variant="contained"
                    color="primary"
                    size="large"
                    disabled={!isValid()}
                >
                    {intl.formatMessage({ defaultMessage: 'Register' })}
                </Button>
                <SocialButtons />
            </FormControl>

            <FormFooter>
                <span> {intl.formatMessage({ defaultMessage: 'Already a member? -' })} </span>
                <Link
                    component={RouterLink}
                    to={'/login'}>
                    {intl.formatMessage({ defaultMessage: 'Login' })}
                </Link>

            </FormFooter>
        </Form>
    );
};

export default RegisterForm;
