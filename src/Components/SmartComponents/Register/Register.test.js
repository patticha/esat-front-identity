import Register from './Register';
import { BrowserRouter as Router } from 'react-router-dom';
import { CAPTCHA_SITE_KEY } from '../../../Utils/constants';
import configureStore from 'redux-mock-store';
import { Provider } from 'react-redux';
import helper from '../../../Utils/useParams';
import { SnackbarProvider } from 'notistack';
import { ReCaptchaProvider } from 'react-recaptcha-x';

jest.mock('react-redux', () => {
    const { Provider, useSelector, useDispatch } = jest.requireActual('react-redux');

    return {
        useDispatch,
        useSelector,
        Provider
    };
});

jest.mock('../../../Utils/useParams', () => ({
    ...jest.requireActual('../../../Utils/useParams'),
    redirect: jest.fn()
}));

const mountComponent = (store, Comp) => {
    return mountWithIntl(
        <ReCaptchaProvider
            siteKeyV3={CAPTCHA_SITE_KEY}
            langCode="en">
            <Provider store={store}>
                <Router>
                    <SnackbarProvider>
                        <Comp />
                    </SnackbarProvider>
                </Router>
            </Provider>
        </ReCaptchaProvider>
    );
};

let mockRegisterStore = {
    register: {
        terms: {
            version: 'v1',
            url: 'www.e-satisfaction.com/terms'
        },
        status: 'accepted',
        errorMessage: undefined
    },
    login: {
        user: {
            token: 'mytoken'
        },
        status: ''
    },
    social: {
        user: {
            token: 'mytoken'
        },
        status: ''
    }
};

const mockStore = configureStore(mockRegisterStore);
const tempStore = mockStore(mockRegisterStore);

beforeEach(() => {
    tempStore.clearActions();
});

describe('[component] Register', () => {

    test('Should render register page', () => {
        const wrapper = mountComponent(tempStore, Register);
        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('Register on success should redirect to login page', () => {
        mockRegisterStore.register.status = 'accepted';
        const tempStore = mockStore(mockRegisterStore);
        mountComponent(tempStore, Register);
        expect(helper.redirect).toHaveBeenCalledTimes(1);
        expect(helper.redirect).toHaveBeenCalledWith('/login');
    });

    test('Register on load should request terms', () => {
        mountComponent(tempStore, Register);
        const actions = tempStore.getActions();
        const expected = [{
            payload: Promise.resolve(),
            type: 'GET_LATEST_TERMS'
        }];
        expect(actions).toEqual(expected);
    });

    test('Register should be able to show password', () => {
        const wrapper = mountComponent(tempStore, Register);
        const icon =  wrapper.find('ForwardRef(IconButton)').first();
        icon.simulate('click');
        wrapper.update();

        const passwrdField = wrapper.find('input#password');
        expect(passwrdField.prop('type')).toBe('text');
    });

    test('Register should be able to check field', () => {
        const wrapper = mountComponent(tempStore, Register);
        let checkBox = wrapper.find('ForwardRef(Checkbox)');
        checkBox.simulate('click', {
            target: {
                name: 'termCheckBox',
                checked: true
            }
        });
        wrapper.update();

        checkBox = wrapper.find('ForwardRef(Checkbox)');
        expect(checkBox.prop('checked')).toBe(false);
    });

    test('Register should display error message', () => {
        mockRegisterStore.register.status = 'error';
        mockRegisterStore.register.errorMessage = 'This is the error';
        const tempStore = mockStore(mockRegisterStore);
        const wrapper = mountComponent(tempStore, Register);
        expect(wrapper.find('FormMessage').prop('error')).toBeTruthy();
        expect(wrapper.find('FormMessage').prop('message')).toBe('This is the error');
    });

    test('Register should submit the form', () => {
        const wrapper = mountComponent(tempStore, Register);
        let form = wrapper.find('form');
        form.simulate('submit');
        const actions = tempStore.getActions();
        const expected = {
            payload: Promise.resolve(),
            type: 'USER_REGISTER'
        };
        expect(actions[1]).toEqual(expected);
    });
});

