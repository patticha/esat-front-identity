/* eslint-disable max-len */
import RecoverPassword from './RecoverPassword';
import { BrowserRouter as Router } from 'react-router-dom';
import { CAPTCHA_SITE_KEY } from '../../../Utils/constants';
import configureStore from 'redux-mock-store';
import { Provider } from 'react-redux';
import { SnackbarProvider } from 'notistack';
import { ReCaptchaProvider } from 'react-recaptcha-x';

jest.mock('react-redux', () => {
    const { Provider, useSelector, useDispatch } = jest.requireActual('react-redux');

    return {
        useDispatch,
        useSelector,
        Provider
    };
});

const mountComponent = (store, Comp) => {
    return mountWithIntl(
        <ReCaptchaProvider
            siteKeyV3={CAPTCHA_SITE_KEY}
            langCode="en">
            <Provider store={store}>
                <Router>
                    <SnackbarProvider>
                        <Comp />
                    </SnackbarProvider>
                </Router>
            </Provider>
        </ReCaptchaProvider>
    );
};

let mockRecoverStore = {
    recover: {
        recover: {
            status: ''
        }
    },
    social: {
        user: {
            token: 'mytoken'
        },
        status: ''
    }
};
const mockStore = configureStore(mockRecoverStore);
const tempStore = mockStore(mockRecoverStore);

beforeEach(() => {
    tempStore.clearActions();
});

describe('Recover', () => {

    test('Should render RecoverPassword page', () => {
        const tempStore = mockStore(mockRecoverStore);
        const wrapper = mountComponent(tempStore, RecoverPassword);
        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('RecoverPassword on load should display default message', () => {
        const tempStore = mockStore(mockRecoverStore);
        const wrapper = mountComponent(tempStore, RecoverPassword);
        expect(wrapper.find('FormTitle p').text()).toBe('Enter your email below to get a password reset link.');
    });

    test('RecoverPassword on success should display message 8 digits', () => {
        mockRecoverStore.recover.recover.status = 'accepted';
        const tempStore = mockStore(mockRecoverStore);
        const wrapper = mountComponent(tempStore, RecoverPassword);
        expect(wrapper.find('FormTitle p').text()).toBe('Click the  link  or enter the  8-digit password  we’ve sent to your email.');
    });

    test('RecoverPassword should display error message', () => {
        mockRecoverStore.recover.recover.status = 'error';
        mockRecoverStore.recover.recover.errorMessage = 'This is the error';
        const tempStore = mockStore(mockRecoverStore);
        const wrapper = mountComponent(tempStore, RecoverPassword);
        expect(wrapper.find('FormMessage').prop('error')).toBeTruthy();
        expect(wrapper.find('FormMessage').prop('message')).toBe('This is the error');
    });

    test('Login should submit the form', () => {
        const wrapper = mountComponent(tempStore, RecoverPassword);
        let form = wrapper.find('form');
        form.simulate('submit');
        const actions = tempStore.getActions();
        const expected = {
            payload: Promise.resolve(),
            type: 'PASSWORD_RECOVER'
        };
        expect(actions).toEqual([expected]);
    });

});
