import React, { Fragment } from 'react';
import { useIntl } from 'react-intl';
import { useDispatch, useSelector } from 'react-redux';
import Link from '@material-ui/core/Link';
import { Link as RouterLink } from 'react-router-dom';
import TextField from '@material-ui/core/TextField';
import Box from '@material-ui/core/Box';
import Divider from '@material-ui/core/Divider';
import Button from '@material-ui/core/Button';
import FormControl from '@material-ui/core/FormControl';
import FormTitle from '../../Presentational/FormTitle/FormTitle';
import FormFooter from '../../Presentational/FormFooter/FormFooter';
import FormMessage from '../../Presentational/FormMessage/FormMessage';
import { STATUS_ACCEPTED, STATUS_ERROR } from '../../../Utils/constants';
import { passwordRecover } from '../../../Store/Actions/actions';
import { Form, useForm } from '../../../Utils/useForm';
import { patterns } from '../../../Helpers/validatorPatterns';
import { useParams } from '../../../Utils/useParams';

const RecoverPassword = () => {
    const dispatch = useDispatch();
    const intl = useIntl();
    const params = useParams();

    const {
        status,
        errorMessage
    } = useSelector(state => state.recover.recover);

    const [
        values,
        errors,
        isValid,
        handleInputChange,
        handleSubmitForm
    ] = useForm(
        {
            email: params.get('email')
        },
        {
            email: [patterns.required, patterns.email]
        }
    );

    const handleSubmit = (e) => {
        const form = handleSubmitForm(e);

        dispatch(
            passwordRecover(
                form.get('email'),
                form.get('captcha')
            )
        );
    };

    return (
        <Form captcha="forgot-password" mxevent='Password Recovery' onSubmit={(e) => handleSubmit(e)}>
            { status === STATUS_ACCEPTED
                ? (
                    <Fragment>
                        <FormTitle
                            title={intl.formatMessage({ defaultMessage: 'Success' })}
                            subTitle={intl.formatMessage(
                                {
                                // eslint-disable-next-line max-len
                                    defaultMessage: 'Click the <b> link </b> or enter the <b> 8-digit password </b> we’ve sent to your email.'
                                },
                                {
                                    b: (chunks) => <b>{chunks}</b>
                                }
                            )}
                        />

                        <Box mt={3} mb={1}></Box>
                        <Button
                            type="submit"
                            fullWidth
                            variant="contained"
                            color="primary"
                            size="large"
                        >
                            <Link
                                style={{ color: '#fff' }}
                                component={RouterLink}
                                to={`/password/reset?email=${values?.email}`}>
                                {intl.formatMessage({ defaultMessage: 'Next' })}
                            </Link>
                        </Button>
                    </Fragment>
                )
                : (
                    <Fragment>
                        <FormTitle
                            title={
                                intl.formatMessage({
                                    defaultMessage: 'Forgot Password?'
                                })
                            }
                            subTitle={
                                intl.formatMessage({
                                    defaultMessage: 'Enter your email below to get a password reset link.'
                                })
                            }
                        />

                        <FormControl fullWidth>

                            <TextField
                                value={values.email}
                                onChange={handleInputChange}
                                variant="outlined"
                                margin="normal"
                                fullWidth
                                id="email"
                                label={intl.formatMessage({ defaultMessage: 'Email' })}
                                name="email"
                                autoComplete="email"
                                autoFocus
                                helperText={errors?.email}
                                error={!!errors?.email}
                            />

                            { status === STATUS_ERROR && <FormMessage error message={errorMessage} /> }

                            <Box mt={3} mb={1}></Box>
                            <Button
                                type="submit"
                                fullWidth
                                variant="contained"
                                color="primary"
                                size="large"
                                disabled={!isValid()}
                            >
                                {intl.formatMessage({ defaultMessage: 'Send' })}

                            </Button>

                            <Box mt={4}>
                                <Divider style={{ margin: 'auto', width: '60%' }} variant="middle"/>
                            </Box>
                        </FormControl>

                        <FormFooter>
                            <span> {intl.formatMessage({ defaultMessage: 'Remembered your password? - ' })} </span>
                            <Link
                                component={RouterLink}
                                to={'/login'}>
                                {intl.formatMessage({ defaultMessage: 'Login' })}
                            </Link>
                        </FormFooter>
                    </Fragment>
                )
            }
        </Form>
    );
};

export default RecoverPassword;
