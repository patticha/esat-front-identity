import React, { useEffect, useState } from 'react';
import { useIntl } from 'react-intl';
import { useDispatch, useSelector } from 'react-redux';
import Link from '@material-ui/core/Link';
import { Link as RouterLink } from 'react-router-dom';
import TextField from '@material-ui/core/TextField';
import IconButton from '@material-ui/core/IconButton';
import FormControl from '@material-ui/core/FormControl';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormHelperText from '@material-ui/core/FormHelperText';
import Checkbox from '@material-ui/core/Checkbox';
import Button from '@material-ui/core/Button';
import Box from '@material-ui/core/Box';
import Grid from '@material-ui/core/Grid';
import { useSnackbar } from 'notistack';
import VisibilityOutlinedIcon from '@material-ui/icons/VisibilityOutlined';
import VisibilityOffOutlinedIcon from '@material-ui/icons/VisibilityOffOutlined';
import FormTitle from '../../Presentational/FormTitle/FormTitle';
import FormFooter from '../../Presentational/FormFooter/FormFooter';
import FormMessage from '../../Presentational/FormMessage/FormMessage';
import SocialButtons from '../SocialButtons/SocialButtons';
import { STATUS_ACCEPTED, STATUS_ERROR, DASHBOARD_URL } from '../../../Utils/constants';
import { Form, useForm } from '../../../Utils/useForm';
import { userLogin, clearStore } from '../../../Store/Actions/actions';
import { patterns } from '../../../Helpers/validatorPatterns';
import { login }  from '../../../Utils/jwt';
import { useParams, redirect } from '../../../Utils/useParams';

const LoginForm = () => {
    const { enqueueSnackbar, closeSnackbar } = useSnackbar();
    const URL = useParams();
    const dispatch = useDispatch();
    const intl = useIntl();
    const [showPassword, setShowPassword] = useState(false);
    const [rememberMe, setRememberMe] = useState(false);

    const {
        user,
        status,
        errorMessage
    } = useSelector(state => state.login);

    const [
        values,
        errors,
        isValid,
        handleInputChange,
        handleSubmitForm
    ] = useForm(
        {
            email: URL.get('email') || '',
            password: ''
        },
        {
            email: [patterns.required, patterns.email],
            password: [patterns.required],
            token: [patterns.required]
        }
    );

    useEffect(() => {
        if (user.twoFactor) {
            enqueueSnackbar(
                intl.formatMessage({
                    defaultMessage: 'Please enter the verification code sent to your assosiated email'
                }),
                {   variant: 'warning' }
            );
        }

    }, [user]);

    useEffect(() => {
        return () => {
            closeSnackbar();
            dispatch(clearStore());
        };
    }, []);

    useEffect(() => {
        if (status === STATUS_ACCEPTED) {
            login(user.token, rememberMe);
            redirect(URL.get('next') || DASHBOARD_URL);
        }

    }, [status]);

    const handleSubmit = (e) => {
        const form = handleSubmitForm(e);
        dispatch(
            userLogin(
                form.get('email'),
                form.get('password'),
                form.get('token'),
                form.get('captcha')
            )
        );
    };

    return (
        <Form captcha="login" mxevent="Login" onSubmit={(e) => handleSubmit(e)}>
            <FormTitle title={intl.formatMessage({ defaultMessage: 'Login' })} />
            <FormControl fullWidth>
                <TextField
                    value={values.email}
                    onChange={handleInputChange}
                    variant="outlined"
                    margin="normal"
                    fullWidth
                    id="email"
                    label={intl.formatMessage({ defaultMessage: 'Email' })}
                    name="email"
                    autoComplete="email"
                    autoFocus
                    helperText={errors?.email}
                    error={!!errors?.email}
                />
                <TextField
                    value={values.password}
                    onChange={handleInputChange}
                    variant="outlined"
                    margin="normal"
                    fullWidth
                    type={showPassword ? 'text' : 'password'}
                    name="password"
                    label={intl.formatMessage({ defaultMessage: 'Password' })}
                    id="password"
                    helperText={errors?.password}
                    error={!!errors?.password}
                    InputProps={{
                        endAdornment:
                        <IconButton onClick={() => setShowPassword(!showPassword)}>
                            {showPassword ? <VisibilityOutlinedIcon /> : <VisibilityOffOutlinedIcon />}
                        </IconButton>
                    }}
                />
                {user.twoFactor &&
                    <TextField
                        value={values.token}
                        onChange={handleInputChange}
                        variant="outlined"
                        margin="normal"
                        fullWidth
                        type={'text'}
                        name="token"
                        label={intl.formatMessage({ defaultMessage: 'Verification Code' })}
                        id="token"
                        helperText={errors?.token}
                        error={!!errors?.token}
                    />
                }

                { status === STATUS_ERROR && <FormMessage error message={errorMessage} /> }

                <Box mt={3} mb={1}>
                    <Grid container>
                        <Grid item xs>
                            <FormControlLabel
                                control={<Checkbox onClick={() => setRememberMe(!rememberMe)} value="remember" color="primary" />}
                                label={
                                    <FormHelperText style={{ fontSize: '14px' }}>
                                        {intl.formatMessage({ defaultMessage: 'Remember me' })}
                                    </FormHelperText>}
                            />
                        </Grid>
                        <Grid item flex alignItems="center" style={{ display: 'inline-flex' }}>
                            <FormHelperText style={{ fontSize: '14px' }}>
                                <Link
                                    component={RouterLink}
                                    to={'/password/recover'}>
                                    {intl.formatMessage({ defaultMessage: 'Forgot?' })}
                                </Link>
                            </FormHelperText>

                        </Grid>
                    </Grid>
                </Box>
                <Button
                    type="submit"
                    fullWidth
                    variant="contained"
                    color="primary"
                    size="large"
                    disabled={!isValid()}
                >
                    {intl.formatMessage({ defaultMessage: 'Login' })}
                </Button>
                <SocialButtons />

            </FormControl>

            <FormFooter>
                <span> {intl.formatMessage({ defaultMessage: 'Not a member yet? -' })} </span>
                <Link
                    component={RouterLink}
                    to={'/register'}>
                    {intl.formatMessage({ defaultMessage: 'Register' })}
                </Link>

            </FormFooter>

        </Form>
    );
};

export default LoginForm;
