import Login from './Login';
import { BrowserRouter as Router } from 'react-router-dom';
import { CAPTCHA_SITE_KEY } from '../../../Utils/constants';
import configureStore from 'redux-mock-store';
import { Provider } from 'react-redux';
import jwt from '../../../Utils/jwt';
import helper from '../../../Utils/useParams';
import { SnackbarProvider } from 'notistack';
import { ReCaptchaProvider } from 'react-recaptcha-x';

jest.mock('react-redux', () => {
    const { Provider, useSelector, useDispatch } = jest.requireActual('react-redux');

    return {
        useDispatch,
        useSelector,
        Provider
    };
});

jest.mock('../../../Utils/jwt', () => ({
    ...jest.requireActual('../../../Utils/jwt'),
    login: jest.fn()
}));

jest.mock('../../../Utils/useParams', () => ({
    ...jest.requireActual('../../../Utils/useParams'),
    redirect: jest.fn()
}));

const mountComponent = (store, Comp) => {
    return mountWithIntl(
        <ReCaptchaProvider
            siteKeyV3={CAPTCHA_SITE_KEY}
            langCode="en">
            <Provider store={store}>
                <Router>
                    <SnackbarProvider>
                        <Comp />
                    </SnackbarProvider>
                </Router>
            </Provider>
        </ReCaptchaProvider>
    );
};

let mockLoginStore = {
    login: {
        user: {
            token: 'mytoken'
        },
        status: ''
    },
    social: {
        user: {
            token: 'mytoken'
        },
        status: ''
    }
};

const mockStore = configureStore(mockLoginStore);
const tempStore = mockStore(mockLoginStore);

beforeEach(() => {
    tempStore.clearActions();
});

describe('Login', () => {

    test('Should render login page', () => {
        const wrapper = mountComponent(tempStore, Login);
        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('Login on success should redirect', () => {
        mockLoginStore.login.status = 'accepted';
        const tempStore = mockStore(mockLoginStore);
        mountComponent(tempStore, Login);
        expect(jwt.login).toHaveBeenCalledTimes(1);
        expect(jwt.login).toHaveBeenCalledWith(mockLoginStore.login.user.token, false);
        expect(helper.redirect).toHaveBeenCalledTimes(1);
        expect(helper.redirect).toHaveBeenCalledWith('https://app.e-satisfaction.com');
    });

    test('Login should display 2 factor notification', () => {
        mockLoginStore.login.user.twoFactor = '2factor';
        const tempStore = mockStore(mockLoginStore);
        const wrapper = mountComponent(tempStore, Login);

        expect(wrapper.find('SnackbarItem')).toHaveLength(1);
        expect(wrapper.find('SnackbarItem').text()).toBe('Please enter the verification code sent to your assosiated email');
    });

    test('Login should display 2 factor text field', () => {
        mockLoginStore.login.user.twoFactor = '2factor';
        const tempStore = mockStore(mockLoginStore);
        const wrapper = mountComponent(tempStore, Login);
        expect(wrapper.find('input#token')).toHaveLength(1);
    });

    test('Login should display error message', () => {
        mockLoginStore.login.status = 'error';
        mockLoginStore.login.errorMessage = 'This is the error';
        const tempStore = mockStore(mockLoginStore);
        const wrapper = mountComponent(tempStore, Login);
        expect(wrapper.find('FormMessage').prop('error')).toBeTruthy();
        expect(wrapper.find('FormMessage').prop('message')).toBe('This is the error');
    });

    test('Login should be able to show password', () => {
        const wrapper = mountComponent(tempStore, Login);
        const icon =  wrapper.find('ForwardRef(IconButton)').first();
        icon.simulate('click');
        wrapper.update();

        const passwrdField = wrapper.find('input#password');
        expect(passwrdField.prop('type')).toBe('text');
    });

    test('Login should be able to change field', () => {
        const wrapper = mountComponent(tempStore, Login);
        let field = wrapper.find('input#email');
        field.simulate('change', {
            target: {
                name: 'email',
                value: 'test@test.com'
            }
        });
        wrapper.update();
        field = wrapper.find('input#email');
        expect(field.prop('value')).toBe('test@test.com');
    });

    test('Login should be able to check field', () => {
        const wrapper = mountComponent(tempStore, Login);
        let checkBox = wrapper.find('ForwardRef(Checkbox)');
        checkBox.simulate('click', {
            target: {
                name: 'remember',
                checked: true
            }
        });
        wrapper.update();

        checkBox = wrapper.find('ForwardRef(Checkbox)');
        expect(checkBox.prop('value')).toBe('remember');
    });

    test('Login should clear store on unmount', () => {
        const wrapper = mountComponent(tempStore, Login);
        wrapper.unmount();
        const actions = tempStore.getActions();
        const expected = {
            payload: {},
            type: 'APP_CLEAR_STORE'
        };
        expect(actions[0]).toEqual(expected);
    });

    test('Login should submit the form', () => {
        const wrapper = mountComponent(tempStore, Login);
        let form = wrapper.find('form');
        form.simulate('submit');
        const actions = tempStore.getActions();
        const expected = {
            payload: Promise.resolve(),
            type: 'USER_LOGIN'
        };
        expect(actions[0]).toEqual(expected);
    });
});

