import React, { useEffect } from 'react';
import { useIntl } from 'react-intl';
import { useDispatch, useSelector } from 'react-redux';
import Button from '@material-ui/core/Button';
import { FacebookProvider, Login } from 'react-facebook';
import { FacebookIcon } from '../../Presentational/CustomIcons/CustomIcons';
import { facebookConnect } from '../../../Store/Actions/actions';
import { STATUS_ACCEPTED, FB_ID, DASHBOARD_URL } from '../../../Utils/constants';
import { useParams, redirect } from '../../../Utils/useParams';
import { login }  from '../../../Utils/jwt';

import useStyles from './SocialButtons.style';

const FacebookButton = () => {
    const intl = useIntl();
    const classes = useStyles();
    const dispatch = useDispatch();
    const URL = useParams();

    const {
        user,
        status
    } = useSelector(state => state.social);

    const handleResponse = (status) => {
        dispatch(facebookConnect(status));
    };

    useEffect(() => {
        if (status === STATUS_ACCEPTED) {
            login(user.token);
            redirect(URL.get('next') || DASHBOARD_URL);
        }

    }, [status]);

    return (
        <FacebookProvider appId={FB_ID}>
            <Login
                scope="email"
                onCompleted={handleResponse}
            >
                {({ handleClick, loading }) => (
                    <Button
                        disabled={loading}
                        color="primary"
                        className={classes.facebook}
                        fullWidth
                        variant="contained"
                        size="large"
                        onClick={handleClick}
                        startIcon={<FacebookIcon />}
                    >
                        {intl.formatMessage({ defaultMessage: 'Continue with Facebook' })}
                    </Button>
                )}
            </Login>
        </FacebookProvider>

    );
};

export default FacebookButton;
