import React, { Fragment } from 'react';
import Grid from '@material-ui/core/Grid';
import Divider from '@material-ui/core/Divider';
import Box from '@material-ui/core/Box';
import FacebookButton from './FacebookButton';
import GoogleButton from './GoogleButton';

const SocialButtons = () => {
    return (
        <Fragment>
            <Box mt={4} mb={4}>
                <Divider style={{ margin: 'auto', width: '60%' }} variant="middle"/>
            </Box>

            <Grid
                container
                direction="column"
                alignItems="stretch"
                spacing={2}
            >
                <Grid item>
                    <FacebookButton />
                </Grid>
                <Grid item>
                    <GoogleButton />
                </Grid>
            </Grid>
        </Fragment>
    );
};

export default SocialButtons;
