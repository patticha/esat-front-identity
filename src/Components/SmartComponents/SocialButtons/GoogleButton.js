import React, { useEffect } from 'react';
import { useIntl } from 'react-intl';
import { useDispatch, useSelector } from 'react-redux';
import Button from '@material-ui/core/Button';
import GoogleLogin from 'react-google-login';
import useStyles from './SocialButtons.style';
import { GoogleIcon } from '../../Presentational/CustomIcons/CustomIcons';
import { googleConnect } from '../../../Store/Actions/actions';
import { GOOGLE_ID, STATUS_ACCEPTED, DASHBOARD_URL } from '../../../Utils/constants';
import { useParams, redirect } from '../../../Utils/useParams';
import { login }  from '../../../Utils/jwt';

const GoogleButton = () => {
    const intl = useIntl();
    const classes = useStyles();
    const dispatch = useDispatch();
    const URL = useParams();

    const {
        user,
        status
    } = useSelector(state => state.social);

    const handleResponse = (status) => {
        dispatch(googleConnect(status));
    };

    useEffect(() => {
        if (status === STATUS_ACCEPTED) {
            login(user.token);
            redirect(URL.get('next') || DASHBOARD_URL);
        }

    }, [status]);

    return (
        <GoogleLogin
            clientId={GOOGLE_ID}
            render={renderProps => (
                <Button
                    className={classes.google}
                    // eslint-disable-next-line react/jsx-handler-names
                    onClick={renderProps.onClick}
                    disabled={renderProps.disabled}
                    type="submit"
                    fullWidth
                    variant="outlined"
                    color="default"
                    size="large"
                    disableRipple
                    startIcon={<GoogleIcon/>}
                >
                    {intl.formatMessage({ defaultMessage: 'Continue with Google' })}
                </Button>
            )}
            buttonText="Login"
            onSuccess={handleResponse}
            cookiePolicy={'single_host_origin'}
        />

    );
};

export default GoogleButton;
