import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(() => ({
    facebook: {
        background: '#3B5998',
        color: '#fff',
        '> img': {
            marginRight: '50px'
        }
    },
    google: {
        color: '#333333'
    }
}));

export default useStyles;
