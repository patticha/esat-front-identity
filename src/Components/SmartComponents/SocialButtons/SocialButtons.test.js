import SocialButtons from './SocialButtons';
import GoogleButton from './GoogleButton';
import FacebookButton from './FacebookButton';
import { BrowserRouter as Router } from 'react-router-dom';
import configureStore from 'redux-mock-store';
import { Provider } from 'react-redux';
import jwt from '../../../Utils/jwt';
import helper from '../../../Utils/useParams';

jest.mock('react-redux', () => {
    const { Provider, useSelector } = jest.requireActual('react-redux');

    return {
        useDispatch: jest.fn(),
        useSelector,
        Provider
    };
});

jest.mock('../../../Utils/jwt', () => ({
    ...jest.requireActual('../../../Utils/jwt'),
    login: jest.fn()
}));

jest.mock('../../../Utils/useParams', () => ({
    ...jest.requireActual('../../../Utils/useParams'),
    redirect: jest.fn()
}));

let mockSocialStore = {
    social: {
        user: {
            token: 'mytoken'
        }
    }
};

const mockStore = configureStore(mockSocialStore);

const mountComponent = (store, Comp) => {
    return  mountWithIntl(
        <Provider store={store}>
            <Router>
                <Comp />
            </Router>
        </Provider>
    );
};

it('Should render SocialButtons', () => {
    const wrapper = shallow(
        <SocialButtons />
    );

    expect(toJson(wrapper)).toMatchSnapshot();
});

test('Should render GoogleButton', () => {
    const tempStore = mockStore(mockSocialStore);
    const wrapper = mountComponent(tempStore, GoogleButton);
    expect(toJson(wrapper)).toMatchSnapshot();
});

test('Google Button should redirect on success', () => {
    mockSocialStore.social.status = 'accepted';
    const tempStore = mockStore(mockSocialStore);
    mountComponent(tempStore, GoogleButton);
    expect(jwt.login).toHaveBeenCalledTimes(1);
    expect(jwt.login).toHaveBeenCalledWith(mockSocialStore.social.user.token);
    expect(helper.redirect).toHaveBeenCalledTimes(1);
    expect(helper.redirect).toHaveBeenCalledWith('https://app.e-satisfaction.com');
});

test('Should render Facebook Button', () => {
    const tempStore = mockStore(mockSocialStore);
    const wrapper = mountComponent(tempStore, FacebookButton);
    expect(toJson(wrapper)).toMatchSnapshot();
});
test('Facebook Button should redirect on success', () => {
    mockSocialStore.social.status = 'accepted';
    const tempStore = mockStore(mockSocialStore);
    mountComponent(tempStore, FacebookButton);
    expect(jwt.login).toHaveBeenCalledTimes(1);
    expect(jwt.login).toHaveBeenCalledWith(mockSocialStore.social.user.token);
    expect(helper.redirect).toHaveBeenCalledTimes(1);
    expect(helper.redirect).toHaveBeenCalledWith('https://app.e-satisfaction.com');
});
