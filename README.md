
# Front Identity

 Front Identity is responsible for displaying the authentication page for e-satisfaction.


## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. 


### Installing

A step by step series, how to get a development env running.

Clone the repo locally

```
git clone git@bitbucket.org:esatisfaction/esat-front-identity.git
```

Install packages & dependencies

```
npm install
```
Install client

```
npm run client
```

### Running Project 

Make sure you have the following entries in your hosts *(/etc/hosts)*

- 127.0.0.1 id.e-satisfaction.localhost

- 127.0.0.1 api.e-satisfaction.localhost


#### Bundle assets
The first step is to bundle all the assets. 
```
npm run build
```
After running this command a `dist` folder, containing all the bundles, will be created.

### Start local server
Next, you need to run the webpack-server

```
npm run start
```
> The ports **8080** should be free

### Mock server

```
npm run mock
```
> The ports **8000** should be free

### Please make sure you read the following
- Application runs with https, due to facebook requirements. You won't be able to access the application with http.
- For testing social connection LOCALLY, it is suggested to use https://localhost:8080/. Otherwise, they will throw an error "Redirect URL doesn't match". 
- Due to mixed content (UI runs on https and mock server runs on http), you should unblock it and disable protection in order to be able to test mock responses
    - [How to Unblock mixed content Firefox](https://support.mozilla.org/en-US/kb/mixed-content-blocking-firefox#w_unblock-mixed-content)


## Internalization 
```
npm run translations
```
The above command will handle all the flow for the internalization. In more details: 
- extract the literals from source code
- compile the default language and store it under /lang/locales
- generate .pot for the default language 

Additionally, it handles the translated files .po,  compiles to `json` and store it under /lang/locales.

## Packages

## Api Clients
Auto generated Javascript clients. 

```
npm run client
```

By running the previous command you

 - install openapi-generator-cli globally
 - generate auth and sys api baced on their specs
 - compile typescript files into .js so it can be consumed by Client.js