const fs = require('fs');
const bodyParser = require('body-parser');
const jsonServer = require('json-server');
// const jwt = require('jsonwebtoken')

const server = jsonServer.create();
const router = jsonServer.router('./database.json');
const userdb = JSON.parse(fs.readFileSync('./users.json', 'UTF-8'));
const ERROR = JSON.parse(fs.readFileSync('./responses/error.json', 'UTF-8'));
const SUCCESS = JSON.parse(fs.readFileSync('./responses/success.json', 'UTF-8'));

server.use(bodyParser.urlencoded({ extended: true }));
server.use(bodyParser.json());
server.use(jsonServer.defaults());

const isAuthenticated = ({ email, password }) => {
    return userdb.users.some(user => user.email === email && user.password === password);
};

const twofactorVerification = ({ email, password, token }) => {
    return userdb.users.some(user => user.email === email && user.password === password && user.token === token);
};

const needs2Factor = ({ email, password }) => {
    let [user] = userdb.users.filter(user => user.email === email && user.password === password);
    return !!(user && user.token);
};

const emailExist = (email) => userdb.users.some(user => user.email === email);

server.post('/@auth/login', (req, res) => {
    const { email, password } = req.body;

    if (!emailExist(email)) {
        const { status, error } = ERROR.LOGIN['404'];

        res.status(status).json({ status, error });
        return;
    }

    if (!isAuthenticated({ email, password })) {
        const { status, error } = ERROR.LOGIN['401'];

        res.status(status).json({ status, error });
        return;
    }

    if (isAuthenticated({ email, password }) && needs2Factor({ email, password })) {
        const { token } = req.body;

        if (token && !twofactorVerification({ email, password, token })) {
            res.status(401).json(ERROR.LOGIN['401']);
            return;
        }
        else if (token && twofactorVerification({ email, password, token })) {
            res.status(200).json(SUCCESS.LOGIN['200']);
            return;
        } else {
            res.status(200).json(SUCCESS.LOGIN_TOKEN['200']);
            return;
        }

    } else {
        res.status(200).json(SUCCESS.LOGIN['200']);
        return;
    }

});

server.post('/@auth/register', (req, res) => {
    const { email } = req.body;

    if (emailExist(email) === true) {
        const { status, error } = ERROR.REGISTER['409'];

        res.status(status).json({ status, error });
        return;
    }

    res.status(201).json(SUCCESS.REGISTER['201']);
    return;
});

server.post('/@auth/password/recover', (req, res) => {
    const { email } = req.body;

    if (emailExist(email) === false) {
        const { status, error } = ERROR.RECOVER['404'];

        res.status(status).json({ status, error });
        return;
    }

    res.status(200).json(SUCCESS.RECOVER['200']);
    return;
});

server.post('/@auth/password/reset', (req, res) => {
    const { email } = req.body;

    if (emailExist(email) === false) {
        const { status, error } = ERROR.RESET['400'];

        res.status(status).json({ status, error });
        return;
    }

    res.status(200).json(SUCCESS.RESET['200']);
    return;
});

server.post('/@auth/oauth/facebook', (req, res) => {

    res.status(200).json(SUCCESS.SOCIAL['200']);
    return;
});

server.post('/@auth/oauth/google', (req, res) => {

    res.status(200).json(SUCCESS.SOCIAL['200']);
    return;
});

server.get('/@sys/terms/latest', (req, res) => {
    res.status(200).json(SUCCESS.TERMS['200']);
    return;
});

server.use(router);

server.listen(8000, () => {
    console.log('Run Auth API Server');
});
