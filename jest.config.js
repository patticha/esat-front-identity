module.exports = {
    clearMocks: true,
    collectCoverage: true,
    collectCoverageFrom: [
        'src/**/*.{js,jsx,mjs}',
        '!**/*style.js',
        '!**/theme.js',
        '!**/Intercom.js',
        '!**/Mixpanel.js',
        '!**/test-helper.js',
        '!**/index.js',
        '!**/Routes.js',
        '!**/entry.js',
        '!**/actions.js',
        '!**/errorMiddleware.js'
    ],
    coverageDirectory: './coverage',
    moduleFileExtensions: ['js', 'json', 'jsx'],
    testEnvironment: 'jsdom',
    testMatch: [
        '**/__tests__/**/*.js?(x)',
        '**/?(*.)+(spec|test).js?(x)'
    ],
    testPathIgnorePatterns: [
        '<rootDir>/dist/',
        '<rootDir>/node_modules/'
    ],
    testURL: 'http://id.e-satisfaction.localhost:8080/',
    transformIgnorePatterns: ['<rootDir>/node_modules/'],
    verbose: true,
    moduleNameMapper: {
        '\\.(jpg|jpeg|png|gif|eot|otf|webp|svg|ttf|woff|woff2|mp4|webm|wav|mp3|m4a|aac|oga)$': '<rootDir>/__mocks__/fileMock.js',
        '\\.(scss|css)$': 'identity-obj-proxy'
    },
    setupFiles: [
        '<rootDir>/config/setupTests.js'
    ],
    globals: {
        BASE_URL: 'http://id.e-satisfaction.localhost:8080/',
        DOMAIN: '.e-satisfaction.com'
    }
};
